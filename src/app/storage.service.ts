import { Injectable } from '@angular/core';
import { Student } from './Models/student';
import { Teacher } from './Models/teacher';
import { User } from './Models/user';
import { ResponseModel } from './Models/response-model';
import { DepartmentTimeDetails } from './Models/department-time-details';
import { Department } from './Models/department';
import { CollegeTimeDetail } from './Models/college-time-detail';
import { StudentTimeTable } from './Models/student-time-table';
import { BasicDetail } from './Models/basic-detail';
import { EducationDetail } from './Models/education-detail';
import { ExperienceDetail } from './Models/experience-detail';
import { StudentTimeTableResponse } from './Models/student-time-table-response';
import { TeacherTimeTableResponse } from './Models/teacher-time-table-response';
import { DepartmentSubject } from './Models/department-subject';
import { ClassAttendance } from './Models/class-attendance';
import { ClassAttendanceResponse } from './Models/class-attendance-response';
import { Quiz } from './Models/quiz';
import { QuizAnswer } from './Models/quiz-answer';
import { Role } from './Models/role';
import { MenuItem } from './Models/menu-item';
import { UserQuizDetail } from './Models/user-quiz-detail';
import { UserSubjectQuiz } from './Models/user-subject-quiz';
import { StudentQuiz } from './Models/student-quiz';
import { StudentClassHistory } from './Models/student-class-history';
import { LectureNotes } from './Models/lecture-notes';
import { TeacherStatus } from './Models/teacher-status';
import { QuestionTypeEnum } from './Models/question-type-enum.enum';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }
  
  setRoles(){
    let existingRoles = window.localStorage.getItem('Roles') ? <Array<Role>>JSON.parse(window.localStorage.getItem('Roles')) : new Array<Role>();
    if(existingRoles.length == 0){
      let studentRole = new Role()
      studentRole.roleID = 1;
      studentRole.roleName = 'Student';
      let teacherRole = new Role()
      teacherRole.roleID = 2;
      teacherRole.roleName = 'Teacher';
      existingRoles.push(studentRole);
      existingRoles.push(teacherRole);
      window.localStorage.setItem('Roles', JSON.stringify(existingRoles));
      console.log('Added Roles')
    }
    else{
      console.log('Roles Already Exists');
    }
    }
    
    setMenuItems(){
      let existingMenuItems = window.localStorage.getItem('MenuItems') ? <Array<MenuItem>>JSON.parse(window.localStorage.getItem('MenuItems')) : new Array<MenuItem>();
      if(existingMenuItems.length == 0){
        let timeTableMenuItem = new MenuItem()
        timeTableMenuItem.menuID = 1;
        timeTableMenuItem.menuName = 'TimeTable';
        timeTableMenuItem.menuPath = '/College/TimeTable';
        timeTableMenuItem.JSONName = 'MenuItems.TimeTable';
        timeTableMenuItem.accessRole = new Array<number>();
        timeTableMenuItem.accessRole.push(1)
        timeTableMenuItem.accessRole.push(2)
        timeTableMenuItem.childMenu = new Array<MenuItem>();
        let quizMenuItem = new MenuItem()
        quizMenuItem.menuID = 2;
        quizMenuItem.menuName = 'Quiz';
        quizMenuItem.JSONName = 'MenuItems.Quiz';
        quizMenuItem.menuPath = '/College/Quiz';
        quizMenuItem.accessRole = new Array<number>();
        quizMenuItem.accessRole.push(1)
        quizMenuItem.accessRole.push(2)
        quizMenuItem.childMenu = new Array<MenuItem>();
        let uploaddMaterialMenuItem = new MenuItem()
        uploaddMaterialMenuItem.menuID = 3;
        uploaddMaterialMenuItem.menuName = 'Upload Material';
        uploaddMaterialMenuItem.JSONName = 'MenuItems.UploadMaterial';
        uploaddMaterialMenuItem.menuPath = '/College/upload';
        uploaddMaterialMenuItem.accessRole = new Array<number>();
        uploaddMaterialMenuItem.accessRole.push(2)
        uploaddMaterialMenuItem.childMenu = new Array<MenuItem>();
        let lecturesMenuItem = new MenuItem()
        lecturesMenuItem.menuID = 4;
        lecturesMenuItem.menuName = 'Lecture Notes';
        lecturesMenuItem.JSONName = 'MenuItems.LectureNotes';
        lecturesMenuItem.menuPath = '/College/lecturenotes';
        lecturesMenuItem.accessRole = new Array<number>();
        lecturesMenuItem.accessRole.push(1)
        lecturesMenuItem.accessRole.push(2)
        lecturesMenuItem.childMenu = new Array<MenuItem>();
        let classHistoryMenuItem = new MenuItem()
        classHistoryMenuItem.menuID = 5;
        classHistoryMenuItem.menuName = 'Class History';
        classHistoryMenuItem.JSONName = 'MenuItems.ClassHistory';
        classHistoryMenuItem.menuPath = '/College/history';
        classHistoryMenuItem.accessRole = new Array<number>();
        classHistoryMenuItem.accessRole.push(1)
        // classHistory.accessRole.push(2)
        classHistoryMenuItem.childMenu = new Array<MenuItem>();
        let dailyStatusItem = new MenuItem()
        dailyStatusItem.menuID = 6;
        dailyStatusItem.menuName = 'Daily Status';
        dailyStatusItem.JSONName = 'MenuItems.DailyStatus';
        dailyStatusItem.menuPath = '/College/dailystatus';
        dailyStatusItem.accessRole = new Array<number>();
        dailyStatusItem.accessRole.push(2)
        // classHistory.accessRole.push(2)
        dailyStatusItem.childMenu = new Array<MenuItem>();
        existingMenuItems.push(timeTableMenuItem);
        existingMenuItems.push(quizMenuItem);
        existingMenuItems.push(uploaddMaterialMenuItem);
        existingMenuItems.push(lecturesMenuItem);
        existingMenuItems.push(classHistoryMenuItem);
        existingMenuItems.push(dailyStatusItem);
        window.localStorage.setItem('MenuItems', JSON.stringify(existingMenuItems));
        console.log('Added Menu Items')
      }
      else{
        console.log('Menu Items Already Exists');
      } 
    }

    getMenuItems(){
      let userRole = this.getUserRole()
      let existingMenuItems = window.localStorage.getItem('MenuItems') ? <Array<MenuItem>>JSON.parse(window.localStorage.getItem('MenuItems')) : new Array<MenuItem>();
      let menuItems = existingMenuItems.filter(x => x.accessRole.find(y => y == userRole));
      return menuItems;
    }

  public addStudent(student: Student, password:string, departmentID: number){
    let existingStudents = window.localStorage.getItem('StudentList') ? <Array<Student>>JSON.parse(window.localStorage.getItem('StudentList')) : new Array<Student>();
    student.studentID = existingStudents.length + 1001;
    existingStudents.push(student);
    window.localStorage.setItem('StudentList', JSON.stringify(existingStudents));
    let user: User = new User();
    user.email = student.basicDetails.emailID;
    user.password = password;
    user.belongsToID = student.studentID;
    user.isTeacher = false;
    user.role = 1;
    this.addUser(user);
    this.setStudentDepartment(student.studentID, departmentID);
  }
  public addTeacher(teacher: Teacher, password:string){
    let existingTeachers = window.localStorage.getItem('TeacherList') ? <Array<Teacher>>JSON.parse(window.localStorage.getItem('TeacherList')) : new Array<Teacher>();
    teacher.teacherID = existingTeachers.length + 101;
    existingTeachers.push(teacher);
    window.localStorage.setItem('TeacherList', JSON.stringify(existingTeachers));
    let user: User = new User();
    user.email = teacher.basicDetails.emailID;
    user.password = password;
    user.belongsToID = teacher.teacherID;
    user.isTeacher = true;
    user.role = 2;
    this.addUser(user);
  }
  private addUser(user: User){
    let existingUsers = window.localStorage.getItem('UserList') ? <Array<User>>JSON.parse(window.localStorage.getItem('UserList')) : new Array<User>();
    user.userID = existingUsers.length + 90001;
    existingUsers.push(user);
    window.localStorage.setItem('UserList', JSON.stringify(existingUsers));
  }

  checkUserCred(user: User){
    let existingUsers = window.localStorage.getItem('UserList') ? <Array<User>>JSON.parse(window.localStorage.getItem('UserList')) : new Array<User>();
    let userDetails = existingUsers.find(x => x.email.toLowerCase() == user.email.toLowerCase());
    if(userDetails != null && userDetails != undefined){
      if(userDetails.password == user.password){
        window.localStorage.setItem('loggedInPersonID', JSON.stringify(userDetails.userID))
        window.localStorage.setItem('loggedInID', JSON.stringify(userDetails.belongsToID))
        window.localStorage.setItem('isTeacher', JSON.stringify(userDetails.isTeacher))
        return new ResponseModel('Credentials Matched', true);
      }
      else{
        return new ResponseModel("Password doesn't match", false);
      }
    }
    else{
      return new ResponseModel("EmailID doesn't match", false);
    }
  }
  getLoggerInID(){
    return parseInt(window.localStorage.getItem('loggedInID'));
  }
  setCollegeTimings(){
    let existingCollegTimings = window.localStorage.getItem('CollegeTimings') ? <Array<CollegeTimeDetail>>JSON.parse(window.localStorage.getItem('CollegeTimings')) : new Array<CollegeTimeDetail>();
    if(existingCollegTimings.length == 0){
    var collegeTimings = new Array<CollegeTimeDetail>();
    collegeTimings.push(new CollegeTimeDetail(1, {hours:9,minutes:0}, {hours:10,minutes:0}));
    collegeTimings.push(new CollegeTimeDetail(2, {hours:10,minutes:0}, {hours:11,minutes:0}));
    collegeTimings.push(new CollegeTimeDetail(3, {hours:11,minutes:0}, {hours:12,minutes:0}));
    collegeTimings.push(new CollegeTimeDetail(4, {hours:12,minutes:0}, {hours:13,minutes:0}));
    collegeTimings.push(new CollegeTimeDetail(5, {hours:13,minutes:0}, {hours:14,minutes:0}));
    collegeTimings.push(new CollegeTimeDetail(6, {hours:14,minutes:0}, {hours:15,minutes:0}));
    collegeTimings.push(new CollegeTimeDetail(7, {hours:15,minutes:0}, {hours:16,minutes:0}));
    console.log(collegeTimings);
    window.localStorage.setItem('CollegeTimings', JSON.stringify(collegeTimings));
    }
    console.log(existingCollegTimings);

  }
  setDepartments(){
    let existingDepartments = window.localStorage.getItem('Departments') ? <Array<Department>>JSON.parse(window.localStorage.getItem('Departments')) : new Array<Department>();
    if(existingDepartments.length == 0){
      let departments: Array<Department> = new Array<Department>();
      departments.push(new Department(1, 'ECE'));
      departments.push(new Department(2, 'CSE'));
      window.localStorage.setItem('Departments',JSON.stringify(departments));
      console.log('Departments Added')
    }
    console.log('Departments were already available')
  }
  setDepartmentSubjects(){
    let existingDepartmentSubjects = window.localStorage.getItem('DepartmentSubjects') ? <Array<DepartmentSubject>>JSON.parse(window.localStorage.getItem('DepartmentSubjects')) : new Array<DepartmentSubject>();
    if(existingDepartmentSubjects.length == 0){
      let departmentSubjects: Array<DepartmentSubject> = new Array<DepartmentSubject>();
      departmentSubjects.push(new DepartmentSubject(0, 'Lunch Break', 0));
      departmentSubjects.push(new DepartmentSubject(1, 'Engineering Optimization', 1));
      departmentSubjects.push(new DepartmentSubject(2, 'SemiConductors', 1));
      departmentSubjects.push(new DepartmentSubject(3, 'Digital Electronics', 1));
      departmentSubjects.push(new DepartmentSubject(4, 'Calculus', 1));
      departmentSubjects.push(new DepartmentSubject(5, 'Image Processing', 1));
      departmentSubjects.push(new DepartmentSubject(6, 'OOPS', 1));
      window.localStorage.setItem('DepartmentSubjects',JSON.stringify(departmentSubjects));
      console.log('Department Subjects Added')
    }
    console.log('Department Subjects were already available')
  }
  setStudentDepartmentsTimeTable(){
    let existingStudentDepartmentTimeTable = window.localStorage.getItem('StudentDepartmentTimeTable') ? <Array<StudentTimeTable>>JSON.parse(window.localStorage.getItem('StudentDepartmentTimeTable')) : new Array<StudentTimeTable>();
    if(existingStudentDepartmentTimeTable.length == 0){
    let studentDepartMentTimeTable = new Array<StudentTimeTable>();
    let departmentsTimeTable: Array<DepartmentTimeDetails> = new Array<DepartmentTimeDetails>();
    departmentsTimeTable.push(new DepartmentTimeDetails(1,105,1))
    departmentsTimeTable.push(new DepartmentTimeDetails(2,107,2))
    departmentsTimeTable.push(new DepartmentTimeDetails(3,108,3))
    departmentsTimeTable.push(new DepartmentTimeDetails(0,0,4))
    departmentsTimeTable.push(new DepartmentTimeDetails(4,106,5))
    departmentsTimeTable.push(new DepartmentTimeDetails(5,109,6))
    departmentsTimeTable.push(new DepartmentTimeDetails(6,101,7))
    studentDepartMentTimeTable.push(new StudentTimeTable(1,1,1,departmentsTimeTable));

    departmentsTimeTable = [];
    departmentsTimeTable.push(new DepartmentTimeDetails(5,109,1))
    departmentsTimeTable.push(new DepartmentTimeDetails(1,105,2))
    departmentsTimeTable.push(new DepartmentTimeDetails(2,107,3))
    departmentsTimeTable.push(new DepartmentTimeDetails(0,0,4))
    departmentsTimeTable.push(new DepartmentTimeDetails(6,101,5))
    departmentsTimeTable.push(new DepartmentTimeDetails(4,106,6))
    departmentsTimeTable.push(new DepartmentTimeDetails(3,108,7));
    studentDepartMentTimeTable.push(new StudentTimeTable(2,1,2,departmentsTimeTable));

    departmentsTimeTable = [];
    departmentsTimeTable.push(new DepartmentTimeDetails(6,101,1))
    departmentsTimeTable.push(new DepartmentTimeDetails(4,106,2))
    departmentsTimeTable.push(new DepartmentTimeDetails(3,108,3));
    departmentsTimeTable.push(new DepartmentTimeDetails(0,0,4))
    departmentsTimeTable.push(new DepartmentTimeDetails(5,109,5))
    departmentsTimeTable.push(new DepartmentTimeDetails(1,105,6))
    departmentsTimeTable.push(new DepartmentTimeDetails(2,107,7))
    studentDepartMentTimeTable.push(new StudentTimeTable(3,1,3,departmentsTimeTable));

    departmentsTimeTable = [];
    departmentsTimeTable.push(new DepartmentTimeDetails(3,108,1));
    departmentsTimeTable.push(new DepartmentTimeDetails(6,101,2))
    departmentsTimeTable.push(new DepartmentTimeDetails(4,106,3))
    departmentsTimeTable.push(new DepartmentTimeDetails(0,0,4))
    departmentsTimeTable.push(new DepartmentTimeDetails(1,105,5))
    departmentsTimeTable.push(new DepartmentTimeDetails(5,109,6))
    departmentsTimeTable.push(new DepartmentTimeDetails(2,107,7))
    studentDepartMentTimeTable.push(new StudentTimeTable(4,1,4,departmentsTimeTable));

    departmentsTimeTable = [];
    departmentsTimeTable.push(new DepartmentTimeDetails(4,106,1))
    departmentsTimeTable.push(new DepartmentTimeDetails(5,109,2))
    departmentsTimeTable.push(new DepartmentTimeDetails(6,101,3))
    departmentsTimeTable.push(new DepartmentTimeDetails(0,0,4))
    departmentsTimeTable.push(new DepartmentTimeDetails(2,107,5))
    departmentsTimeTable.push(new DepartmentTimeDetails(1,105,6))
    departmentsTimeTable.push(new DepartmentTimeDetails(3,108,7));
    studentDepartMentTimeTable.push(new StudentTimeTable(5,1,5,departmentsTimeTable));

    // departmentsTimeTable = [];
    // departmentsTimeTable.push(new DepartmentTimeDetails('OOPS',101,1))
    // departmentsTimeTable.push(new DepartmentTimeDetails('DBMS',102,2))
    // departmentsTimeTable.push(new DepartmentTimeDetails('Cloud Computing',103,3))
    // departmentsTimeTable.push(new DepartmentTimeDetails('Lunch Break',0,4))
    // departmentsTimeTable.push(new DepartmentTimeDetails('Digital Design',104,5))
    // departmentsTimeTable.push(new DepartmentTimeDetails('Engineering Optimization',105,6))
    // departmentsTimeTable.push(new DepartmentTimeDetails('Calculus',106,7))
    // studentDepartMentTimeTable.push(new StudentTimeTable(6,2,1,departmentsTimeTable))
    window.localStorage.setItem('StudentDepartmentTimeTable', JSON.stringify(studentDepartMentTimeTable))
    }
  }
  setStudents(){
    let existingStudents = window.localStorage.getItem('StudentList') ? <Array<Student>>JSON.parse(window.localStorage.getItem('StudentList')) : new Array<Student>();
    if(existingStudents.length == 0)
    {
      for(let i = 1; i<=20; i++){
        let basicDetails: BasicDetail = new BasicDetail();
        basicDetails.firstName = 'Student'
        basicDetails.lastName = i.toString()
        basicDetails.profileName = basicDetails.firstName + ' ' + basicDetails.lastName;
        basicDetails.emailID = 'student'+i+'@ggk.com';
        basicDetails.addressDetails = 'Address' + i;
        basicDetails.addressArea= 'Student ' +i+' Area';
        basicDetails.city = 'Hyderabad';
        basicDetails.state = 'Telangana'
        basicDetails.pincode = 500038;
        let education: EducationDetail = new EducationDetail();
        education.schoolPercentage = 90+i;
        education.intermediatePercentage = 85+i;
        education.ugPercentage = 80+i
        let student: Student = new Student();
        student.basicDetails = basicDetails ;
        student.educationDetials = education ;
        student.IsActive = true;
        this.addStudent(student, 'Welcome2ggk', i%2 == 0 ? 1 : 1)
      }
    }    
    
  }
  setTeachers(){
    let existingCollegTimings = window.localStorage.getItem('TeacherList') ? <Array<Teacher>>JSON.parse(window.localStorage.getItem('TeacherList')) : new Array<Teacher>();
    if(existingCollegTimings.length == 0){
      for(let i = 1; i<=9; i++){
        let basicDetails: BasicDetail = new BasicDetail();
        basicDetails.firstName = 'Teacher'
        basicDetails.lastName = i.toString()
        basicDetails.profileName = basicDetails.firstName + ' ' + basicDetails.lastName;
        basicDetails.emailID = 'teacher'+i+'@ggk.com';
        basicDetails.addressDetails = 'Address' + i;
        basicDetails.addressArea= 'teacher ' +i+' Area';
        basicDetails.city = 'Hyderabad';
        basicDetails.state = 'Telangana'
        basicDetails.pincode = 500038;
        let education: EducationDetail = new EducationDetail();
        education.schoolPercentage = 90+i;
        education.intermediatePercentage = 85+i;
        education.ugPercentage = 80+i
        let experience: ExperienceDetail = new ExperienceDetail();
        experience.subject = 'Subject '+ i;
        experience.experience = 3+i;
        let teacher: Teacher = new Teacher();
        teacher.basicDetails = basicDetails ;
        teacher.educationDetials = education ;
        teacher.experienceDetails = experience;
        teacher.IsActive = true;
        this.addTeacher(teacher,'Welcome2ggk');
      }
    }
    
    
  }
  setStudentDepartment(studentID: number, departmentID: number){
    let existingStudentDepartment = window.localStorage.getItem('StudentDepartments') ? <Array<{studentID:number, departmentID: number}>>JSON.parse(window.localStorage.getItem('StudentDepartments')) : new Array<{studentID:number, departmentID: number}>();
    existingStudentDepartment.push({studentID:studentID, departmentID:departmentID});
    window.localStorage.setItem('StudentDepartments', JSON.stringify(existingStudentDepartment));
  }
  getStudentTimeTable(studentID: number, weekDay: number): Array<StudentTimeTableResponse>{
    let collegTimings = window.localStorage.getItem('CollegeTimings') ? <Array<CollegeTimeDetail>>JSON.parse(window.localStorage.getItem('CollegeTimings')) : new Array<CollegeTimeDetail>();
    let teachers = window.localStorage.getItem('TeacherList') ? <Array<Teacher>>JSON.parse(window.localStorage.getItem('TeacherList')) : new Array<Teacher>();
    let departmentSubjects = window.localStorage.getItem('DepartmentSubjects') ? <Array<DepartmentSubject>>JSON.parse(window.localStorage.getItem('DepartmentSubjects')) : new Array<DepartmentSubject>();
    let studentDepartment = window.localStorage.getItem('StudentDepartments') ? (<Array<{studentID:number, departmentID: number}>>JSON.parse(window.localStorage.getItem('StudentDepartments'))) : new Array<{studentID:number, departmentID: number}>();
    let departmentTimeTable: StudentTimeTable = window.localStorage.getItem('StudentDepartmentTimeTable') ? <StudentTimeTable>(<Array<StudentTimeTable>>JSON.parse(window.localStorage.getItem('StudentDepartmentTimeTable'))).find(x => x.departmentID == studentDepartment.find(y => y.studentID == studentID).departmentID && x.weekDay == weekDay) : new StudentTimeTable();
    console.log(departmentTimeTable)
    if(departmentTimeTable == undefined || departmentTimeTable == null){
      return null;
    }
    let studentTimeTable = new Array<StudentTimeTableResponse>();
    for(var i =0; i< departmentTimeTable.timeDetails.length; i++){
      let periodDetails = new StudentTimeTableResponse
      periodDetails.subjectID = departmentTimeTable.timeDetails[i].subjectID;
      periodDetails.subjectName = departmentSubjects.find(x =>x.subjectID == departmentTimeTable.timeDetails[i].subjectID).subjectName;
      periodDetails.teacherID = departmentTimeTable.timeDetails[i].teacherID;
      periodDetails.teacherName = teachers.find(x=> x.teacherID == departmentTimeTable.timeDetails[i].teacherID)?teachers.find(x=> x.teacherID == departmentTimeTable.timeDetails[i].teacherID).basicDetails.profileName:'-';
      periodDetails.From = {hours:collegTimings.find(x => x.timeID == departmentTimeTable.timeDetails[i].timeID).startTime.hours, minutes:collegTimings.find(x => x.timeID == departmentTimeTable.timeDetails[i].timeID).startTime.minutes};
      periodDetails.To = {hours:collegTimings.find(x => x.timeID == departmentTimeTable.timeDetails[i].timeID).endTime.hours, minutes:collegTimings.find(x => x.timeID == departmentTimeTable.timeDetails[i].timeID).endTime.minutes};
      studentTimeTable.push(periodDetails);
    }
    console.log(studentTimeTable)
    return studentTimeTable
  }
  getTeacherTimeTable(date:Date, teacherID?: number) {
    let weekDay = date.getDay();
    let departments = window.localStorage.getItem('Departments') ? <Array<Department>>JSON.parse(window.localStorage.getItem('Departments')) : new Array<Department>();
    let collegTimings = window.localStorage.getItem('CollegeTimings') ? <Array<CollegeTimeDetail>>JSON.parse(window.localStorage.getItem('CollegeTimings')) : new Array<CollegeTimeDetail>();
    let departmentSubjects = window.localStorage.getItem('DepartmentSubjects') ? <Array<DepartmentSubject>>JSON.parse(window.localStorage.getItem('DepartmentSubjects')) : new Array<DepartmentSubject>();
    let departmentTimeTables: Array<StudentTimeTable> = window.localStorage.getItem('StudentDepartmentTimeTable') ? (<Array<StudentTimeTable>>JSON.parse(window.localStorage.getItem('StudentDepartmentTimeTable'))).filter(x =>  x.weekDay == weekDay) : new Array<StudentTimeTable>();
    if(departmentTimeTables == undefined || departmentTimeTables == null || departmentTimeTables.length == 0){
      return null;
    }
    let teacherTimeTable = new Array<TeacherTimeTableResponse>();
    for(var j = 0; j<departmentTimeTables.length; j++){
      for(var i =0; i< departmentTimeTables[j].timeDetails.length; i++){
        if(departmentTimeTables[j].timeDetails[i].teacherID == teacherID){
          let periodDetails = new TeacherTimeTableResponse
          periodDetails.subjectID = departmentTimeTables[j].timeDetails[i].subjectID;
          periodDetails.departmentID = departmentTimeTables[j].departmentID;
          periodDetails.subjectName = departmentSubjects.find(x => x.subjectID == departmentTimeTables[j].timeDetails[i].subjectID).subjectName;
          periodDetails.className = departments.find(x=> x.departmentID == departmentTimeTables[j].departmentID)?departments.find(x=> x.departmentID == departmentTimeTables[j].departmentID).departmentName:'-';
          periodDetails.From = {hours:collegTimings.find(x => x.timeID == departmentTimeTables[j].timeDetails[i].timeID).startTime.hours, minutes:collegTimings.find(x => x.timeID == departmentTimeTables[j].timeDetails[i].timeID).startTime.minutes};
          periodDetails.To = {hours:collegTimings.find(x => x.timeID == departmentTimeTables[j].timeDetails[i].timeID).endTime.hours, minutes:collegTimings.find(x => x.timeID == departmentTimeTables[j].timeDetails[i].timeID).endTime.minutes};
          let attendanceList = window.localStorage.getItem('ClassAtendance') ? <Array<ClassAttendance>>JSON.parse(window.localStorage.getItem('ClassAtendance')) : new Array<ClassAttendance>();
          if(attendanceList.length>0){
            let classAttendanceList = attendanceList.filter(x => x.classDate == date.toLocaleDateString() && x.subjectID == periodDetails.subjectID && x.teacherID == teacherID && x.classID == periodDetails.departmentID)
            if(classAttendanceList.length>0){
              periodDetails.isAttendanceTaken =true;
            }
          }
          
          teacherTimeTable.push(periodDetails);
        }
        else if(departmentTimeTables[j].timeDetails[i].teacherID == 0){
          let periodDetails = new TeacherTimeTableResponse
          periodDetails.subjectName = departmentSubjects.find(x => x.subjectID == departmentTimeTables[j].timeDetails[i].subjectID).subjectName;
          periodDetails.className = '-';
          periodDetails.From = {hours:collegTimings.find(x => x.timeID == departmentTimeTables[j].timeDetails[i].timeID).startTime.hours, minutes:collegTimings.find(x => x.timeID == departmentTimeTables[j].timeDetails[i].timeID).startTime.minutes};
          periodDetails.To = {hours:collegTimings.find(x => x.timeID == departmentTimeTables[j].timeDetails[i].timeID).endTime.hours, minutes:collegTimings.find(x => x.timeID == departmentTimeTables[j].timeDetails[i].timeID).endTime.minutes};
          teacherTimeTable.push(periodDetails);
        }
      }
    }
    for(var i =0; i< collegTimings.length; i++){
      if(teacherTimeTable.find(x => x.From.hours == collegTimings[i].startTime.hours) == undefined ){
        let periodDetails = new TeacherTimeTableResponse
        periodDetails.subjectName = '-';
        periodDetails.className = '-'
        periodDetails.From = {hours:collegTimings[i].startTime.hours, minutes:collegTimings[i].startTime.minutes};
        periodDetails.To = {hours:collegTimings[i].endTime.hours, minutes:collegTimings[i].endTime.minutes};
        teacherTimeTable.push(periodDetails);
      }
    }
    teacherTimeTable = teacherTimeTable.sort((x,y) => x.From.hours-y.From.hours);
    return teacherTimeTable;
  }

 getStudentDepartment(studentID: number){
  let departments = window.localStorage.getItem('Departments') ? <Array<Department>>JSON.parse(window.localStorage.getItem('Departments')) : new Array<Department>();
  let studentDepartmentID = window.localStorage.getItem('StudentDepartments') ? (<Array<{studentID:number, departmentID: number}>>JSON.parse(window.localStorage.getItem('StudentDepartments'))).find(y => y.studentID == studentID).departmentID : 0;
  let studentDepartment = ''
  if(studentDepartmentID != 0){
    studentDepartment = departments.find(x => x.departmentID == studentDepartmentID).departmentName
  }
  return studentDepartment
 }

 getStudentDepartmentID(studentID: number){
  let departments = window.localStorage.getItem('Departments') ? <Array<Department>>JSON.parse(window.localStorage.getItem('Departments')) : new Array<Department>();
  let studentDepartmentID = window.localStorage.getItem('StudentDepartments') ? (<Array<{studentID:number, departmentID: number}>>JSON.parse(window.localStorage.getItem('StudentDepartments'))).find(y => y.studentID == studentID).departmentID : 0;
  return studentDepartmentID
 }
 getStudentsByDepartmentClassForAttendance(departmentID: number){
  let studentDepartment = window.localStorage.getItem('StudentDepartments') ? <Array<{studentID:number, departmentID: number}>>JSON.parse(window.localStorage.getItem('StudentDepartments')) : new Array<{studentID:number, departmentID: number}>();
  let existingStudents = window.localStorage.getItem('StudentList') ? <Array<Student>>JSON.parse(window.localStorage.getItem('StudentList')) : new Array<Student>();
  let departmentStudents = studentDepartment.filter(x => x.departmentID == departmentID);
  let students = new Array<ClassAttendanceResponse>();
  for(var i = 0; i < departmentStudents.length; i++){
    let student = new ClassAttendanceResponse();
    student.studentID = departmentStudents[i].studentID
    student.attended = false;
    student.studentName = existingStudents.find(x => x.studentID == departmentStudents[i].studentID).basicDetails.profileName;
    students.push(student);
  }
  return students;
 }
 getClassAttendance(classID: number, lecturerID: number, classDate: Date, subjectID: number){
  let attendanceList = window.localStorage.getItem('ClassAtendance') ? <Array<ClassAttendance>>JSON.parse(window.localStorage.getItem('ClassAtendance')) : new Array<ClassAttendance>();
  let classAttendanceList = attendanceList.filter(x => x.classDate == classDate.toLocaleDateString() && x.subjectID == subjectID && x.teacherID == lecturerID && x.classID == classID)
  let existingStudents = window.localStorage.getItem('StudentList') ? <Array<Student>>JSON.parse(window.localStorage.getItem('StudentList')) : new Array<Student>();
  let attendanceResponseList = new Array<ClassAttendanceResponse>();
  for(var i = 0; i< classAttendanceList.length; i++){
    let attendanceResponse = new ClassAttendanceResponse();
    attendanceResponse.studentID = classAttendanceList[i].studentID;
    attendanceResponse.attended = classAttendanceList[i].attended;
    attendanceResponse.studentName = existingStudents.find(x => x.studentID == classAttendanceList[i].studentID).basicDetails.profileName;
    attendanceResponseList.push(attendanceResponse);
  }
  return attendanceResponseList
}
 addClassAttendance(classID: number, lecturerID: number, classDate: Date, subjectID: number, attendanceData: Array<ClassAttendanceResponse>){
  let attendanceList = window.localStorage.getItem('ClassAtendance') ? <Array<ClassAttendance>>JSON.parse(window.localStorage.getItem('ClassAtendance')) : new Array<ClassAttendance>();
  if(attendanceData != null && attendanceData != undefined)
  {
    if(attendanceList.length == 0 ){
      for(var i = 0; i< attendanceData.length; i++){
        let attendance = new ClassAttendance();
        attendance.studentID = attendanceData[i].studentID;
        attendance.attended = attendanceData[i].attended;
        attendance.subjectID = subjectID;
        attendance.classDate = classDate.toLocaleDateString();
        attendance.teacherID = lecturerID;
        attendance.classID = classID;
        attendanceList.push(attendance);
      }
      window.localStorage.setItem('ClassAtendance', JSON.stringify(attendanceList));
      return true;
    }
    else if(attendanceList.filter(x => x.classDate == classDate.toLocaleDateString() && x.subjectID == subjectID && x.teacherID == lecturerID && x.classID == classID).length > 0){
      var studentsList = attendanceList.filter(x => x.classDate == classDate.toLocaleDateString() && x.subjectID == subjectID && x.teacherID == lecturerID && x.classID == classID);
      for(var i = 0; i < attendanceData.length; i++ ){
        if(studentsList.find(x => x.studentID == attendanceList[i].studentID) != undefined){
          attendanceList.find(x => x.classDate == classDate.toLocaleDateString() && x.subjectID == subjectID && x.teacherID == lecturerID && x.classID == classID && x.studentID == attendanceList[i].studentID).attended =attendanceData[i].attended;
        }
        else{
          let attendance = new ClassAttendance();
          attendance.studentID = attendanceData[i].studentID;
          attendance.attended = attendanceData[i].attended;
          attendance.subjectID = subjectID;
          attendance.classDate = classDate.toLocaleDateString();
          attendance.teacherID = lecturerID;
          attendance.classID = classID;
          attendanceList.push(attendance);
        }
      }
      window.localStorage.setItem('ClassAtendance', JSON.stringify(attendanceList));
      return true;
    }
    else{
      for(var i = 0; i< attendanceData.length; i++){
        let attendance = new ClassAttendance();
        attendance.studentID = attendanceData[i].studentID;
        attendance.attended = attendanceData[i].attended;
        attendance.subjectID = subjectID;
        attendance.classDate = classDate.toLocaleDateString();
        attendance.teacherID = lecturerID;
        attendance.classID = classID;
        attendanceList.push(attendance);
      }
      window.localStorage.setItem('ClassAtendance', JSON.stringify(attendanceList));
      return true;
    }
   
  }
  
}
  addQuiz(quizData: Quiz){
    let existingQuizData = window.localStorage.getItem('QuizData') ? <Array<Quiz>>JSON.parse(window.localStorage.getItem('QuizData')) : new Array<Quiz>();
    
    if(existingQuizData.length == 0){
      quizData.quizID = existingQuizData.length + 1;
      existingQuizData.push(quizData);
    }
    else{
      let index =  existingQuizData.findIndex(x => x.quizID == quizData.quizID)
      if(index >=0){
        existingQuizData[index] = quizData;
      }
      else{
        quizData.quizID = existingQuizData.length + 1;
        existingQuizData.push(quizData);
      }
    }
    window.localStorage.setItem('QuizData', JSON.stringify(existingQuizData));
    return quizData.quizID;
  }
  getQuizListLength(){
    let existingQuizData = window.localStorage.getItem('QuizData') ? <Array<Quiz>>JSON.parse(window.localStorage.getItem('QuizData')) : new Array<Quiz>();
    return existingQuizData.length;
  }
  getQuizData(quizID: number){
    let existingQuizData = window.localStorage.getItem('QuizData') ? <Array<Quiz>>JSON.parse(window.localStorage.getItem('QuizData')) : new Array<Quiz>();
    return existingQuizData.find(x => x.quizID == quizID);
  }
  addQuizAnswers(quizAnswers: QuizAnswer){
    let existingQuizAnswers = window.localStorage.getItem('QuizAnswers') ? <Array<QuizAnswer>>JSON.parse(window.localStorage.getItem('QuizAnswers')) : new Array<QuizAnswer>();
    if(existingQuizAnswers.length == 0){
      existingQuizAnswers.push(quizAnswers);
    }
    else{
      let index =  existingQuizAnswers.findIndex(x => x.quizID == quizAnswers.quizID)
      if(index >=0){
        existingQuizAnswers[index] = quizAnswers;
      }
      else{
        existingQuizAnswers.push(quizAnswers);
      }
    }
    window.localStorage.setItem('QuizAnswers', JSON.stringify(existingQuizAnswers));

  }
  getUserSubjectQuizDetails(){
    let userRole = this.getUserRole()
      let quizDetails = new Array<UserSubjectQuiz>();
      if(userRole == 2){
        return this.getQuizDetailsOfTeacher();
      }
      else if(userRole == 1){
        let studentDepartmentID = this.getStudentDepartmentID(1001);
        let existingDepartmentSubjects = this.getDepartmentSubjects(studentDepartmentID)
        if(existingDepartmentSubjects == null){
          return quizDetails;
        }
        else if(existingDepartmentSubjects.length == 0){
          return quizDetails;
        }
        else{
          for(var i = 0; i< existingDepartmentSubjects.length; i++){
            let quiz = new UserSubjectQuiz();
            quiz.subjectID = existingDepartmentSubjects[i].subjectID
            quiz.subjectName = existingDepartmentSubjects[i].subjectName
            quiz.quizDetails = this.getUserQuizDetailsBySubjectID(quiz.subjectID);
            if(quiz.quizDetails.length > 0){
              quizDetails.push(quiz);
            }
          }
          return quizDetails;
        }
      }
  }
  calculateQuizScore(quizID: number, studentAnswers: QuizAnswer){
    let existingQuizAnswers = window.localStorage.getItem('QuizAnswers') ? <Array<QuizAnswer>>JSON.parse(window.localStorage.getItem('QuizAnswers')) : new Array<QuizAnswer>();
    let quizAnswers = existingQuizAnswers.find(x => x.quizID == quizID)
    if(quizAnswers == undefined){
      return null;
    }
    let quizData = this.getQuizData(quizID);
    let marks = 0;
    for(var i = 0; i < quizData.sections.length; i++){
      let sectionWeightage = quizData.sections[i].weightage;
      let correctAnswers = 0;
      for(var j = 0; j < quizData.sections[i].questions.length; j++){
        if(quizData.sections[i].questions[j].questionType == QuestionTypeEnum.RadioType){
          if(quizAnswers.sections[i].questions[j].radioTypeanswers == studentAnswers.sections[i].questions[j].radioTypeanswers){
            correctAnswers = correctAnswers + 1;
          }
        }
        else if(quizData.sections[i].questions[j].questionType == QuestionTypeEnum.CheckBoxType){
          var answerFlag = 0;
          for(var k = 0; k<quizAnswers.sections[i].questions[j].checkBoxTypeAnswers.length; k++){
            if(quizAnswers.sections[i].questions[j].checkBoxTypeAnswers[k].isAnswer == studentAnswers.sections[i].questions[j].checkBoxTypeAnswers[k].isAnswer){
              answerFlag = 1;
            }
            else{
              answerFlag = 0;
              break;
            }
          }
          if(answerFlag == 1){
            correctAnswers = correctAnswers + 1;
          }
        }
      }
      marks = marks + (correctAnswers/quizData.sections[i].questions.length) * sectionWeightage;
      console.log(marks, ' ', correctAnswers , ' ', quizData.sections[i].questions.length, ' ', sectionWeightage )
    }
    return marks;
  }
  getDepartmentSubjects(departmentID: number){
    let existingDepartmentSubjects = window.localStorage.getItem('DepartmentSubjects') ? <Array<DepartmentSubject>>JSON.parse(window.localStorage.getItem('DepartmentSubjects')) : new Array<DepartmentSubject>();
    if(existingDepartmentSubjects.length == 0){
      return null;
    }
    else{
      let departmentSubjects = existingDepartmentSubjects.filter(x => x.departmentID == departmentID);
      return departmentSubjects;
    }
  }
  getUserQuizDetailsBySubjectID(subjectID: number){
    let quizDataList = new Array<UserQuizDetail>();
    let existingQuizData = window.localStorage.getItem('QuizData') ? <Array<Quiz>>JSON.parse(window.localStorage.getItem('QuizData')) : new Array<Quiz>();
    let existingStudentQuizAnswers = window.localStorage.getItem('StudentQuizAnswers') ? <Array<StudentQuiz>>JSON.parse(window.localStorage.getItem('StudentQuizAnswers')) : new Array<StudentQuiz>();
    if(existingQuizData.length > 0){
      let subjectQuizData = existingQuizData.filter(x => x.subjectID == subjectID);
      if(subjectQuizData.length > 0){
        for(var i =0; i<subjectQuizData.length;i++){
          let quizData = new UserQuizDetail();
          quizData.quizID = subjectQuizData[i].quizID;
          quizData.answersProvided = subjectQuizData[i].answersGiven;
          quizData.quizLabel = subjectQuizData[i].quizLabel;
          quizData.totalMarks = subjectQuizData[i].totalMarks;
          if(existingStudentQuizAnswers.length == 0){
            quizData.quizTaken = false;
          }
          else{
            let studentAnswers = existingStudentQuizAnswers.find(x => x.quizID == quizData.quizID && x.studentID == 1001)
            if(studentAnswers != undefined){
              quizData.quizTaken = true;
              let marks = this.calculateQuizScore(quizData.quizID, studentAnswers.answers);
              console.log(marks);
              if(marks == null){
                quizData.marks =  Number.MAX_VALUE;
              }
              else{
                quizData.marks =  marks;
              }
            }
            else{
              quizData.quizTaken = false;
            }
          }
          quizDataList.push(quizData);
        }        
      }
    }
    return quizDataList;
  }
  getUserRole(){
    let loggedInUserId = window.localStorage.getItem('loggedInPersonID');
    let existingUsers = window.localStorage.getItem('UserList') ? <Array<User>>JSON.parse(window.localStorage.getItem('UserList')) : new Array<User>();
    let roleID = existingUsers.find(x => x.userID == parseInt(loggedInUserId)).role
    return roleID;
    //roleID;
  }
  addStudentQuizAnswers(quizAnswers: QuizAnswer, studentID?: number){
    if(studentID == null){
      studentID = 1001;
    }
    let existingStudentQuizAnswers = window.localStorage.getItem('StudentQuizAnswers') ? <Array<StudentQuiz>>JSON.parse(window.localStorage.getItem('StudentQuizAnswers')) : new Array<StudentQuiz>();
    let quiz = new StudentQuiz();
    quiz.quizID = quizAnswers.quizID;
    quiz.studentID = studentID;
    quiz.answers = quizAnswers;
    if(existingStudentQuizAnswers.length == 0){
      existingStudentQuizAnswers.push(quiz);
    }
    else{
      let index =  existingStudentQuizAnswers.findIndex(x => x.quizID == quizAnswers.quizID && x.studentID == quiz.studentID)
      if(index >=0){
        existingStudentQuizAnswers[index] = quiz;
      }
      else{
        existingStudentQuizAnswers.push(quiz);
      }
    }
    window.localStorage.setItem('StudentQuizAnswers', JSON.stringify(existingStudentQuizAnswers));
   
  }

  getStudentQuizAnswersByQuizID(quizID: number, studentID?: number){
    let existingStudentQuizAnswers = window.localStorage.getItem('StudentQuizAnswers') ? <Array<StudentQuiz>>JSON.parse(window.localStorage.getItem('StudentQuizAnswers')) : new Array<StudentQuiz>();
    let quizAnswers = new QuizAnswer();
    if(studentID == null || studentID == undefined){
      studentID = 1001;
    }
    var studentQuizAnswers = existingStudentQuizAnswers.find(x => x.quizID == quizID && x.studentID == studentID);
    if(studentQuizAnswers != undefined){
      quizAnswers = studentQuizAnswers.answers;
    }
    else{
      return null;
    }
    return quizAnswers;
  }

  getStudentClassHistory(studentID?: number){
    if(studentID == null || studentID == undefined){
      studentID = 1001;
    }
    var studentDepartmentID = this.getStudentDepartmentID(studentID);
    var classHistoryList = new Array<StudentClassHistory>();
    var todayDate = new Date();
    for(var i = 1; i<= 30; i ++){
      todayDate.setDate(todayDate.getDate() - 1);
      let classes = this.getStudentTimeTable(studentID, todayDate.getDay());
      if(classes != null){
        for(var j = 0; j < classes.length; j++){
          if(classes[j].subjectID == 0){
            continue;
          }
          var classHistory = new StudentClassHistory();
          classHistory.subjectID = classes[j].subjectID;
          classHistory.subjectName = classes[j].subjectName;
          classHistory.teacherName = classes[j].teacherName;
          classHistory.classDate = todayDate.toLocaleDateString();
          var status = this.getStudentClassStatus(studentDepartmentID, 101, todayDate, classHistory.subjectID, studentID);
          classHistory.status = status == null?'Pending Attendance':status == false?'Not Attended':'Attended';
          classHistoryList.push(classHistory);
        }
      }
    }
    console.clear();
    console.log(classHistoryList)
    return classHistoryList
  }
  getStudentClassStatus(classID: number, lecturerID: number, classDate: Date, subjectID: number, studentID: number){
    let attendanceList = window.localStorage.getItem('ClassAtendance') ? <Array<ClassAttendance>>JSON.parse(window.localStorage.getItem('ClassAtendance')) : new Array<ClassAttendance>();
    let classAttendanceList = attendanceList.filter(x => x.classDate == classDate.toLocaleDateString() && x.subjectID == subjectID && x.teacherID == lecturerID && x.classID == classID)
    let studentData = classAttendanceList.find(x => x.studentID == studentID);
    if(studentData == undefined){
      return null;
    }
  return studentData.attended;
  }

  getLectureNotesForStudents(){
   
  }

  getLectureNotesOfTeacher(date: Date, teacherID?: number){
    if(teacherID == null){
      teacherID = 101;
    }
    let timeTable = this.getTeacherTimeTable(date, teacherID)?this.getTeacherTimeTable(date, teacherID).filter(x => x.subjectID > 0): null;
    if(timeTable == null){
      return null;
    }
    let notes = this.getLectureNotesOfTeacherOfADay(teacherID, date);
    if(timeTable.length == notes.length){
      return notes;
    }
    let departments = window.localStorage.getItem('Departments') ? <Array<Department>>JSON.parse(window.localStorage.getItem('Departments')) : new Array<Department>();
    let teachers = window.localStorage.getItem('TeacherList') ? <Array<Teacher>>JSON.parse(window.localStorage.getItem('TeacherList')) : new Array<Teacher>();
    let lectureNotesList = new Array<LectureNotes>();
    for(var i = 0; i< timeTable.length; i++){
      if(notes.find(x => x.subjectID == timeTable[i].departmentID) != undefined){
        lectureNotesList.push(notes.find(x => x.subjectID == timeTable[i].subjectID));
      }
      else{
        let lectureNotes = new LectureNotes();
        lectureNotes.classDate = date.toLocaleDateString();
        lectureNotes.departmentID = timeTable[i].departmentID;
        lectureNotes.departmentName = departments.find(x => x.departmentID = lectureNotes.departmentID).departmentName;
        lectureNotes.subjectID = timeTable[i].subjectID;
        lectureNotes.subjectName = timeTable[i].subjectName;
        lectureNotes.teacherID = teacherID;
        lectureNotes.teacherName = teachers.find(x => x.teacherID == teacherID).basicDetails.profileName;
        lectureNotes.notes = undefined;
        lectureNotes.lectureNotesID = 0;
        lectureNotesList.push(lectureNotes);
      }
      
    }
    return lectureNotesList;

  }

  getLectureNotesOfTeacherOfADay(teacherID: number, date: Date){
    let lectureNotesList = window.localStorage.getItem('LectureNotes') ? <Array<LectureNotes>>JSON.parse(window.localStorage.getItem('LectureNotes')) : new Array<LectureNotes>();
    if(lectureNotesList.length == 0){
      return new Array<LectureNotes>();
    }
    else{
      return lectureNotesList.filter(x => x.classDate === date.toLocaleDateString() && x.teacherID == teacherID)
    }
  }

  saveNotesOfTeacher(notes: LectureNotes){
    let lectureNotesList = window.localStorage.getItem('LectureNotes') ? <Array<LectureNotes>>JSON.parse(window.localStorage.getItem('LectureNotes')) : new Array<LectureNotes>();
    notes.notes.trim();
    if(lectureNotesList.length == 0){
      notes.lectureNotesID = lectureNotesList.length + 1;
      lectureNotesList.push(notes);
    }
    else{
      let index = lectureNotesList.findIndex(x => x.lectureNotesID == notes.lectureNotesID)
      if(index >= 0){
        lectureNotesList[index] = notes;
      }
      else{
        notes.lectureNotesID = lectureNotesList.length + 1;
        lectureNotesList.push(notes);
      }
    }
    window.localStorage.setItem('LectureNotes', JSON.stringify(lectureNotesList));
  }

  getTechearDailyStatus(date: Date, teacherID?: number){
    if(teacherID == null){
      teacherID = 101;
    }
    let lectureNotesList = window.localStorage.getItem('TeacherStatus') ? <Array<TeacherStatus>>JSON.parse(window.localStorage.getItem('TeacherStatus')) : new Array<TeacherStatus>();
    let teacherStatus = new Array<TeacherStatus>();
    if(lectureNotesList.length == 0){
      let status = new TeacherStatus();
      status.teacherStatusID = 0;
      status.teacherID = teacherID;
      status.statusDate = date.toLocaleDateString();
      status.statusNotes = undefined;
      teacherStatus.push(status);
      return teacherStatus;
    }
    else{
      teacherStatus =  lectureNotesList.filter(x => x.statusDate === date.toLocaleDateString() && x.teacherID == teacherID)
      if(teacherStatus.length !=0){
        return teacherStatus;
      }
      let status = new TeacherStatus();
      status.teacherStatusID = 0;
      status.teacherID = teacherID;
      status.statusDate = date.toLocaleDateString();
      status.statusNotes = undefined;
      teacherStatus.push(status);
      return teacherStatus;
    }
  }

  saveDailyStatusOfTeacher(notes: TeacherStatus){
    let teacherStatusList = window.localStorage.getItem('TeacherStatus') ? <Array<TeacherStatus>>JSON.parse(window.localStorage.getItem('TeacherStatus')) : new Array<TeacherStatus>();
    notes.statusNotes.trim();
    if(teacherStatusList.length == 0){
      notes.teacherStatusID = teacherStatusList.length + 1;
      teacherStatusList.push(notes);
    }
    else{
      let index = teacherStatusList.findIndex(x => x.teacherStatusID == notes.teacherStatusID)
      if(index >= 0){
        teacherStatusList[index] = notes;
      }
      else{
        notes.teacherStatusID = teacherStatusList.length + 1;
        teacherStatusList.push(notes);
      }
    }
    window.localStorage.setItem('TeacherStatus', JSON.stringify(teacherStatusList));
  }

  getLectureNotesForStudent(date: Date, studentID?: number){
    if(studentID == null){
      studentID = 1001;
    }
    let table = this.getStudentTimeTable(studentID, date.getDay())
    let timeTable = table?table.filter(x => x.subjectID > 0): null;
    if(timeTable == null){
      return null;
    }
    let lectureNotesList = new Array<LectureNotes>();
    for(var i = 0; i< timeTable.length; i++){
      let notes = this.getLectureNotesOfsubjectOfADay(timeTable[i].teacherID, timeTable[i].subjectID, date);
      if(notes.find(x => x.subjectID == timeTable[i].subjectID) != undefined){
        lectureNotesList.push(notes.find(x => x.subjectID == timeTable[i].subjectID));
      }
      else{
        let lectureNotes = new LectureNotes();
        lectureNotes.classDate = date.toLocaleDateString();
        lectureNotes.subjectID = timeTable[i].subjectID;
        lectureNotes.subjectName = timeTable[i].subjectName;
        lectureNotes.teacherID = timeTable[i].teacherID;
        lectureNotes.teacherName = timeTable[i].teacherName;
        lectureNotes.notes = undefined;
        lectureNotesList.push(lectureNotes);
      }
      
    }
    return lectureNotesList;
  }

  getLectureNotesOfsubjectOfADay(teacherID: number,subjectID: number, date: Date){
    let lectureNotesList = window.localStorage.getItem('LectureNotes') ? <Array<LectureNotes>>JSON.parse(window.localStorage.getItem('LectureNotes')) : new Array<LectureNotes>();
    if(lectureNotesList.length == 0){
      return new Array<LectureNotes>();
    }
    else{
      return lectureNotesList.filter(x => x.classDate === date.toLocaleDateString() && x.teacherID == teacherID && x.subjectID == subjectID)
    }
  }


  getQuizDetailsOfTeacher(teacherID?: number){
    let teacherSubjectQuizDetails = new Array<UserSubjectQuiz>();
    if(teacherID == null){
      teacherID = 101;
    }
    let existingQuizData = window.localStorage.getItem('QuizData') ? <Array<Quiz>>JSON.parse(window.localStorage.getItem('QuizData')) : new Array<Quiz>();
    if(existingQuizData.length == 0){
      return null;
    }
    let teacherSubjects = this.getTeacherSubjects(teacherID)
    if(teacherSubjects == null){
      return null;
    }
    let existingQuizAnswers = window.localStorage.getItem('QuizAnswers') ? <Array<QuizAnswer>>JSON.parse(window.localStorage.getItem('QuizAnswers')) : new Array<QuizAnswer>();
    for(var i = 0; i< teacherSubjects.length; i++){
      let quizDetails = existingQuizData.filter(x => x.subjectID == teacherSubjects[i].subjectID)
      if(quizDetails.length != 0){
        let subjectQuiz = new UserSubjectQuiz();
        subjectQuiz.subjectID = teacherSubjects[i].subjectID
        subjectQuiz.subjectName = teacherSubjects[i].subjectName
        let quizDataList = new Array<UserQuizDetail>();
        for(var j = 0; j<quizDetails.length; j++){
          let quiz = new UserQuizDetail();
          quiz.quizID = quizDetails[j].quizID;
          quiz.quizLabel =quizDetails[j].quizLabel;
          quiz.answersProvided = existingQuizAnswers.find(x => x.quizID == quiz.quizID)?true:false;
          quiz.totalMarks = quizDetails[j].totalMarks;
          quizDataList.push(quiz);
        }
        subjectQuiz.quizDetails = quizDataList;
        teacherSubjectQuizDetails.push(subjectQuiz);
      }
    }
    return teacherSubjectQuizDetails;
  }

  getTeacherSubjects(teacherID?: number){
    if(teacherID == null){
      teacherID = 101;
    }
    let departmentSubjects = window.localStorage.getItem('DepartmentSubjects') ? <Array<DepartmentSubject>>JSON.parse(window.localStorage.getItem('DepartmentSubjects')) : new Array<DepartmentSubject>();
    let departmentTimeTables: Array<StudentTimeTable> = window.localStorage.getItem('StudentDepartmentTimeTable') ? (<Array<StudentTimeTable>>JSON.parse(window.localStorage.getItem('StudentDepartmentTimeTable'))) : new Array<StudentTimeTable>();
    if(departmentTimeTables.length == 0){
      return null;
    }
    let teacherSubjects = new Array<DepartmentSubject>();
    for(var i = 0; i< departmentTimeTables.length; i++){
      let data = departmentTimeTables[i].timeDetails.filter(x => x.teacherID == teacherID);
      if(data.length != 0){
        for(var j = 0; j< data.length; j++){
          if(teacherSubjects.find(x => x.subjectID == data[j].subjectID) != undefined){
            continue
          }
          let subjectID = data[j].subjectID
          let subjectName = departmentSubjects.find(x => x.subjectID == subjectID).subjectName
          let departmentID = departmentSubjects.find(x => x.subjectID == subjectID).departmentID
          let subject = new DepartmentSubject(subjectID, subjectName, departmentID);
          teacherSubjects.push(subject);
        }
      }
    }
    return teacherSubjects;
  }

  getQuizAnswersByQuizID(quizID: number){
    let existingQuizAnswers = window.localStorage.getItem('QuizAnswers') ? <Array<QuizAnswer>>JSON.parse(window.localStorage.getItem('QuizAnswers')) : new Array<QuizAnswer>();
    if(existingQuizAnswers.length == 0){
      return null
    }    
      let quizAnswers = new QuizAnswer();
      var answers = existingQuizAnswers.find(x => x.quizID == quizID);
      if(answers != undefined){
        quizAnswers = answers;
      }
      else{
        return null;
      }
      return quizAnswers;
  }

  isLoggedIn(){
    let loggedInUserId = window.localStorage.getItem('loggedInPersonID');
    if(loggedInUserId == null || loggedInUserId == undefined){
      return false;
    }
    else{
    let existingUsers = window.localStorage.getItem('UserList') ? <Array<User>>JSON.parse(window.localStorage.getItem('UserList')) : new Array<User>();
    let userExists = existingUsers.find(x => x.userID == parseInt(loggedInUserId))
    if(userExists != undefined){
      return true
    }
    }
  }
  isAccessibleRoute(routePath: string){
    let userRole = this.getUserRole()
    let existingMenuItems = window.localStorage.getItem('MenuItems') ? <Array<MenuItem>>JSON.parse(window.localStorage.getItem('MenuItems')) : new Array<MenuItem>();
    let routeMenu = existingMenuItems.find(x => x.menuPath == routePath && x.accessRole.find(role => role == userRole))
    if(routeMenu != undefined){
      return true;
    }
    return false;
  }
  getLoggegInUserName(){
    let loggedInUserRole = this.getUserRole();
    let loggedInID = this.getLoggerInID();
    if(loggedInUserRole == 1){
      let existingStudents = window.localStorage.getItem('StudentList') ? <Array<Student>>JSON.parse(window.localStorage.getItem('StudentList')) : new Array<Student>();
      return existingStudents.find(x => x.studentID == loggedInID).basicDetails.profileName;
    }
    else if(loggedInUserRole == 2){
    let existingTeachers = window.localStorage.getItem('TeacherList') ? <Array<Teacher>>JSON.parse(window.localStorage.getItem('TeacherList')) : new Array<Teacher>();
      return  existingTeachers.find(x => x.teacherID == loggedInID).basicDetails.profileName;
    }
  }
  logout(){
    window.localStorage.removeItem('loggedInPersonID')
    window.localStorage.removeItem('loggedInID')
    window.localStorage.removeItem('isTeacher')
  }
}
