import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentLectureNotesComponent } from './student-lecture-notes.component';

describe('StudentLectureNotesComponent', () => {
  let component: StudentLectureNotesComponent;
  let fixture: ComponentFixture<StudentLectureNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentLectureNotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentLectureNotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
