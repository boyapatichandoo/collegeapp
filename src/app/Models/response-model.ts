export class ResponseModel {
    message: string;
    isSuccess: boolean;
    constructor(message: string, isSuccess: boolean){
        this.message = message;
        this.isSuccess = isSuccess;
    }
}
