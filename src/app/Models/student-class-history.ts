export class StudentClassHistory {
    subjectID: number;
    subjectName: string;
    status: string;
    teacherID: number;
    teacherName: string;
    classDate: string;
}
