import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './college/dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatButtonModule} from '@angular/material';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule, MatInputModule,MatSelectModule, MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule}  from '@angular/forms'
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { LoginModule } from './login/login.module';
import { CollegeModule} from './college/college.module'
import { TimeTableComponent} from './college/time-table/time-table.component'
import { CreateQuizComponent} from './college/quiz/create-quiz/create-quiz.component'
import { CreateQuestionComponent} from './college/quiz/create-question/create-question.component'
import { ChartsModule} from 'ng2-charts';
import {MatTooltipModule} from '@angular/material/tooltip';
import { QuestionComponent} from './college/quiz/question/question.component';
import { UnaccessibleComponent } from './unaccessible/unaccessible.component'



@NgModule({
  declarations: [
    AppComponent,
    UnaccessibleComponent,
    // DashboardComponent,
    // TimeTableComponent,
    // CreateQuizComponent,
    // CreateQuestionComponent,
    // QuestionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatCardModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    LoginModule,
    ChartsModule,
    MatTooltipModule,
    MatSelectModule,
    CollegeModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

