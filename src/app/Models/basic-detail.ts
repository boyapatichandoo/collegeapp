export class BasicDetail {
    firstName: string;
    lastName: string;
    profileName: string;
    emailID: string;
    addressDetails: string;
    addressArea: string;
    city: string;
    state: string;
    pincode: number;
}
