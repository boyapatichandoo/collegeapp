export class EducationDetail {
    schoolPercentage: number;
    intermediatePercentage: number;
    ugPercentage: number;
}
