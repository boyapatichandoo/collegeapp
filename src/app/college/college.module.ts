import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollegeComponent } from './college.component';
import { TimeTableComponent } from './time-table/time-table.component';
import { ChartsModule} from 'ng2-charts'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatButtonModule, MatSelectModule} from '@angular/material';
import { MatFormFieldModule, MatInputModule, MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import { HttpClientModule }    from '@angular/common/http';
import { CreateQuizComponent } from './quiz/create-quiz/create-quiz.component';
import { QuestionComponent } from './quiz/question/question.component';
import { CreateQuestionComponent } from './quiz/create-question/create-question.component';
import { DashboardComponent } from './dashboard/dashboard.component'
import { FormsModule } from '@angular/forms';
import{CollegeRoutingModule} from './college-routing.module';
import { QuizComponent } from './quiz/quiz/quiz.component';
import { UploadMaterialComponent } from './upload-material/upload-material.component';
import { ClassHistoryComponent } from './class-history/class-history.component';
import { LectureNotesComponent } from './lecture-notes/lecture-notes.component';
import { DailyStatusComponent } from './daily-status/daily-status.component';
import { StudentLectureNotesComponent } from './student-lecture-notes/student-lecture-notes.component'
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClient} from '@angular/common/http';

@NgModule({
  declarations: [CollegeComponent, DashboardComponent, TimeTableComponent, CreateQuizComponent, QuestionComponent, CreateQuestionComponent, QuizComponent, UploadMaterialComponent, ClassHistoryComponent, LectureNotesComponent, DailyStatusComponent, StudentLectureNotesComponent],
  imports: [
    FormsModule,
    CommonModule,
    ChartsModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatCardModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    CollegeRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    })
],
})
export class CollegeModule { }


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}