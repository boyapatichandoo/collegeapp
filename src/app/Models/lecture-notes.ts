export class LectureNotes {
    lectureNotesID: number;
    subjectID: number;
    subjectName: string;
    teacherID: number;
    teacherName: string;
    departmentID: number;
    departmentName: string;
    classDate: string;
    notes: any;
}
