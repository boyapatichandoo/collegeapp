import { Component, OnInit, ViewChild } from '@angular/core';
import { StorageService } from 'src/app/storage.service';
import { MenuItem } from 'src/app/Models/menu-item';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  menu: string = 'MenuItems.TimeTable';

  showMenu: boolean = false;
  hideMenu: boolean = false;
  toggleMenuFlag: boolean = false;
  userMenuItems: Array<MenuItem>;
  languages: any[];
  selectedLanguage: string;
  userName: string;
  userIconLabel: string;
  userIconClick: boolean = false;
  @ViewChild('childComponent',{static: false})
  child: any
  constructor(private storageService: StorageService, private translate: TranslateService, private router: Router) {
    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');

     // the lang to use, if the lang isn't available, it will use the current loader to get them
    translate.use('en');
    this.userMenuItems = new Array<MenuItem>();
    this.languages = [{name:'English',value:'en'}, {name:'Russian',value:'rs'}];
    this.selectedLanguage=this.languages[0].value;
   }

  ngOnInit() {
    this.userMenuItems = this.storageService.getMenuItems();
    this.userName =this.storageService.getLoggegInUserName();
    this,this.createUserIconLabel();
  }

  createUserIconLabel(){
    
    var firstLetters = this.userName.match(/\b(\w)/g); 
    this.userIconLabel = firstLetters.join(''); // JSON 
  }

  showFullMenu(){
    this.showMenu = !this.showMenu;
    if(this.toggleMenuFlag){
      this.hideMenu = !this.hideMenu;
    }
    else{
      this.hideMenu = false;
    }
  }
  toggleMenuChange(){
    if(this.showMenu){
      this.hideMenu = false;
    }
    else{
      this.hideMenu = !this.hideMenu;
    }
  }
  changeLanguage(){
    console.log(this.selectedLanguage)
    this.translate.use(this.selectedLanguage);
  }
  logout(){
    this.storageService.logout();
    this.router.navigate(['SignIn']);
  }
}
