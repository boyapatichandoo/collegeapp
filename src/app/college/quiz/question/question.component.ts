import { Component, OnInit, Input } from '@angular/core';
import { StorageService } from 'src/app/storage.service';
import { Quiz } from 'src/app/Models/quiz';
import { QuizAnswer } from 'src/app/Models/quiz-answer';
import { SectionAnswer } from 'src/app/Models/section-answer';
import { QuestionAnswer } from 'src/app/Models/question-answer';
import { OptionAnswer } from 'src/app/Models/option-answer';
import {ActivatedRoute, Router} from "@angular/router";
import { QuestionTypeEnum } from 'src/app/Models/question-type-enum.enum';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  quiz: Quiz;
  quizAnswers: QuizAnswer;
  answersValidationFlag: boolean;
  quizID: number;
  routeSub: any;
  userRole: number;
  quizTaken: boolean;
  studentMarks: number;
  constructor(private storageService: StorageService, private route: ActivatedRoute, private router: Router) {
    this.answersValidationFlag = true;
    this.quizTaken = false;
    
   }

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.quizID = +params['id']; 
   });
   console.log(this.quizID)
   this.quiz = this.storageService.getQuizData(this.quizID);
   this.userRole = this.storageService.getUserRole();
   if(this.userRole == 1){
    this.quizAnswers = this.storageService.getStudentQuizAnswersByQuizID(this.quizID);
   }
   else if(this.userRole == 2){
    this.quizAnswers = this.storageService.getQuizAnswersByQuizID(this.quizID);
   }
   if(this.quizAnswers == null){
    this.createQuizAnswerObject();
   }
   else if(this.userRole == 1 && this.quizAnswers != null){
    this.quizTaken = true;
    this.studentMarks = this.storageService.calculateQuizScore(this.quizID, this.quizAnswers);
   }
  }

  createQuizAnswerObject(){
    this.quizAnswers = new QuizAnswer();
    this.quizAnswers.quizID = this.quiz.quizID;
    this.quizAnswers.sections = new Array<SectionAnswer>();
    for(var i = 0; i< this.quiz.sections.length;i++){
      let newSection = new SectionAnswer();
      newSection.sectionID = this.quiz.sections[i].sectionID;
      newSection.questions = new Array<QuestionAnswer>();
      for(var j = 0; j< this.quiz.sections[i].questions.length;j++){
        let newQuestion = new QuestionAnswer();
        newQuestion.questionID = this.quiz.sections[i].questions[j].questionID;
        if(this.quiz.sections[i].questions[j].questionType == QuestionTypeEnum.RadioType){
        newQuestion.radioTypeanswers = '';
        }
        else if(this.quiz.sections[i].questions[j].questionType == QuestionTypeEnum.CheckBoxType){
          newQuestion.checkBoxTypeAnswers = new Array<OptionAnswer>();
          for(var k = 0; k< this.quiz.sections[i].questions[j].questionOptions.length; k++){
            let newOption = new OptionAnswer();
            newOption.optionID = this.quiz.sections[i].questions[j].questionOptions[k].optionID;
            newOption.isAnswer = false;
            newQuestion.checkBoxTypeAnswers.push(newOption);
          }
        }
        console.log( QuestionTypeEnum.CheckBoxType);
       
        newSection.questions.push(newQuestion);
      }
      this.quizAnswers.sections.push(newSection);
    }
  }
  see(){
    console.log(this.quizAnswers)
  }

  addQuizAnswers(){
    this.validateAnswers();
    if(!this.answersValidationFlag){
      return;
    }
    this.storageService.addQuizAnswers(this.quizAnswers);
    this.router.navigate(['College', 'Quiz'])

  }

  addStudentQuizAnswers(){
    this.storageService.addStudentQuizAnswers(this.quizAnswers);
  }

  validateAnswers(){
    this.answersValidationFlag = true;
    var checked = false;
    for(var i = 0; i< this.quizAnswers.sections.length;i++){
      for(var j = 0; j< this.quizAnswers.sections[i].questions.length;j++){
        var answers;
        if(this.quiz.sections[i].questions[j].questionType == QuestionTypeEnum.RadioType){
          answers = this.quizAnswers.sections[i].questions[j].radioTypeanswers;
          }
        else if(this.quiz.sections[i].questions[j].questionType == QuestionTypeEnum.CheckBoxType){
          answers = this.quizAnswers.sections[i].questions[j].checkBoxTypeAnswers.filter(x => x.isAnswer == true);
        }
            if(answers == undefined || answers == null){
          checked = true;
          this.answersValidationFlag = false;
          break;
        }
        else if(answers.length == 0){
          checked = true;
          this.answersValidationFlag = false;
          break;
        }
      }
      if(checked == true){
        break;
      }
    }
  }
  onItemChange(value){
    console.log(" Answer is : ", this.quizAnswers );
    console.log(" Value is : ", value );
 }
}
