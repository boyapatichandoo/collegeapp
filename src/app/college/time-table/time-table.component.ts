import { Component, OnInit } from '@angular/core';
import { StudentTimeTableResponse } from 'src/app/Models/student-time-table-response';
import { StorageService } from 'src/app/storage.service';
import { TeacherTimeTableResponse } from 'src/app/Models/teacher-time-table-response';
import { ClassAttendanceResponse } from 'src/app/Models/class-attendance-response';
import {MatTableDataSource} from '@angular/material/table';
import { MatSort } from '@angular/material';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.css']
})
export class TimeTableComponent implements OnInit {
  // public pieChartDataSets:Array<{ label: string, data: Array<any> }> = []

  // public pieChartLabels:string[] = ['one', 'two', 'three','four','five','six','seven','eight','nine','ten','eleven','twelve'];
  // public pieChartData:number[] = [1,1,1,1,1,1,1,1,1,1,1,1];
  // public pieChartType:string = 'pie';
  // // public pieChartColors:string[] = [''];
  selectedDate: Date;
  timeTable: Array<StudentTimeTableResponse>;
  teahcerTimeTable: Array<TeacherTimeTableResponse>;
  departmentName: string
  currentTime: string;
  isTeacher: boolean = false;
  loggedInID: number;
  studentsAttendanceList: Array<ClassAttendanceResponse>;
  viewAttendance: boolean = false;
  attendedStudentCount: number = 0;
  selectedClassForAttendance: TeacherTimeTableResponse;

  displayedColumnsForTeacher: string[] = ['subjectName', 'departmentName', 'notes'];
  displayedColumnsForStudent: string[] = ['subjectName', 'teacherName', 'notes'];
  teacherDataSource :MatTableDataSource<TeacherTimeTableResponse>;
  studentDataSource :MatTableDataSource<StudentTimeTableResponse>;

  constructor(private storageService: StorageService, private translate: TranslateService) { 
    this.selectedDate = new Date();
    this.studentsAttendanceList = new Array<ClassAttendanceResponse>();
    this.getCurrentTime();
    setInterval(()=>{this.getCurrentTime()},1000);
  //   translate.onLangChange.subscribe(lang=>{
  //     this.changeLanguage(lang.lang);
  // })
  }
  
  ngOnInit() {
    this.loggedInID = this.storageService.getLoggerInID();
    this.isTeacher = this.storageService.getUserRole() == 2 ? true: false;
    this.getTimeTableDetails();
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.changeLanguage(event.lang);
  });
  }

  getTimeTableDetails(){
    if(this.isTeacher){
      this.teahcerTimeTable = this.storageService.getTeacherTimeTable(this.selectedDate, this.loggedInID);
    }
    else{
      this.timeTable = this.storageService.getStudentTimeTable(this.loggedInID, this.selectedDate.getDay());
      this.departmentName = this.storageService.getStudentDepartment(this.loggedInID);
    }
  }

  getPeriodTimings(hoursFrom: number, hoursTo: number){
      var timings = hoursFrom/12 >= 1 ?(hoursFrom%12 == 0? '12 PM-' : hoursFrom%12 + ' PM- ') : hoursFrom + ' AM- ';
      timings += hoursTo/12 >= 1 ? (hoursTo%12 == 0? '12 PM' : hoursTo%12 + ' PM') : hoursTo + ' AM';
      return timings;
  }
  getCurrentTime(){
    this.currentTime = new Date().toLocaleString();
  }
  periodStatus(fromHours: number, fromMinutes: number,toHours: number, toMinutes: number){
    let presentTime = new Date();
    let presentDayTmeInSec = new Date().getHours()*3600 + new Date().getMinutes()*60;
    let periodFromTimeInSec = fromHours*3600 + fromMinutes*60;
    let periodToTimeInSec = toHours*3600 + toMinutes*60;
    if(this.selectedDate.getFullYear() == presentTime.getFullYear()){
      if(this.selectedDate.getMonth() < presentTime.getMonth()){
        return 'Completed';
      }
      else if(this.selectedDate.getMonth() > presentTime.getMonth()){
        return 'Not Started';
      }
      else if(this.selectedDate.getMonth() == presentTime.getMonth()){
        if(this.selectedDate.getDate() < presentTime.getDate()){
          return 'Completed';
        }
        else if(this.selectedDate.getDate() > presentTime.getDate()){
          return 'Not Started';
        }
        else{
          if(periodFromTimeInSec <= presentDayTmeInSec && periodToTimeInSec > presentDayTmeInSec){
            return 'Running';
          }
          else if(periodFromTimeInSec > presentDayTmeInSec){
            return 'Not Started';
          }
          else if(periodToTimeInSec < presentDayTmeInSec){
            return 'Completed'
          }
          else{
            '-'
          }
        }
      }
    }
    else if(this.selectedDate.getFullYear() < presentTime.getFullYear()){
      return 'Completed';
    }
    else if(this.selectedDate.getFullYear() > presentTime.getFullYear()){
      return 'Not Started';
    }
 
  }
  getStudentsListForAttendance(value: TeacherTimeTableResponse){
    this.selectedClassForAttendance = value;
    this.studentsAttendanceList = this.storageService.getStudentsByDepartmentClassForAttendance(value.departmentID);
    this.viewAttendance = true;
    this.countAttendedValue();
  }
  countAttendedValue(){
    setTimeout(()=>{this.attendedStudentCount = this.studentsAttendanceList.filter(x =>x.attended).length}, 0);
  }
  getClassAttendance(value: TeacherTimeTableResponse){
    this.selectedClassForAttendance = value;
    this.studentsAttendanceList = this.storageService.getClassAttendance(value.departmentID, 101, this.selectedDate, value.subjectID);
    this.viewAttendance = true;
    this.countAttendedValue();
    
  }
  saveClassAttendance(){
    var status = this.storageService.addClassAttendance(this.selectedClassForAttendance.departmentID, 101, this.selectedDate, this.selectedClassForAttendance.subjectID, this.studentsAttendanceList);
    this.teahcerTimeTable.find(x => x.subjectID == this.selectedClassForAttendance.subjectID).isAttendanceTaken = status
  }
  // events
  // public chartClicked(e:any):void {
  //   console.log(e);
  // }
 
  // public chartHovered(e:any):void {
  //   console.log(e);
  // }

  changeLanguage(language: string){
    console.log(language)
    this.translate.use(language);

  }
}
