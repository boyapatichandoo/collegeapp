import { Role } from './role';

export class MenuItem {
    menuID: number;
    menuName: string;
    childMenu: Array<MenuItem>;
    accessRole: Array<number>;
    menuPath: string;
    JSONName: string;
}

