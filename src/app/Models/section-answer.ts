import { QuestionAnswer } from './question-answer';

export class SectionAnswer {
    sectionID: number;
    questions: Array<QuestionAnswer>;
}
