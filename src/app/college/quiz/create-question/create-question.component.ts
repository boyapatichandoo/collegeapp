import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { QuestionSection } from 'src/app/Models/question-section';
import { Question } from 'src/app/Models/question';
import { QuestionOption } from 'src/app/Models/question-option';
import { Quiz } from 'src/app/Models/quiz';
import { StorageService } from 'src/app/storage.service';
import { DepartmentSubject } from 'src/app/Models/department-subject';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class CreateQuestionComponent implements OnInit {

  quiz: Quiz;
  quizSection: Array<QuestionSection>;
  validationFlag: boolean;
  quizAdded: boolean;
  teacherSubjects: Array<DepartmentSubject>;
  constructor(private storageService: StorageService, private router: Router) {
    this.quiz = new Quiz()
    // this.quiz.quizID = this.storageService.getQuizListLength() + 1;
    this.quizSection = new Array<QuestionSection>();
    this.validationFlag = false;
    this.quizAdded = false;
    this.teacherSubjects = new Array<DepartmentSubject>();
    if(this.storageService.getUserRole() != 2){
      this.router.navigate['no-access'];
    }
   }

  ngOnInit() {
    this.teacherSubjects = this.storageService.getTeacherSubjects();
  }

  addSection(){
    let newSection = new QuestionSection();
    newSection.questions = new Array<Question>();
    newSection.sectionID = this.quizSection.length + 1;
    this.quizSection.push(newSection);
    if(this.quiz.sectionsValidationFlag == false){
      this.removeQuizSectionCountValidation();
    }
  }

  addSectionQuestion(sectionID: number){
    let newQuestion = new Question();
    newQuestion.questionOptions = new Array<QuestionOption>()
    newQuestion.questionID = this.quizSection.find(x => x.sectionID == sectionID).questions.length + 1;
    this.quizSection.find(x => x.sectionID == sectionID)
    .questions
    .push(newQuestion)
    let sectionIndex = this.quizSection.findIndex(x => x.sectionID == sectionID)
    if(this.quizSection[sectionIndex].questionsValidationFlag == false){
      this.removeQuizSectionQuestionCountValidation(sectionIndex);
    }
  }

  addSectionQuestionOption(sectionID: number, questionID: number){
    let newQuestionOption = new QuestionOption();
    newQuestionOption.optionID = this.quizSection.find(x => x.sectionID == sectionID)
                                  .questions
                                  .find(y => y.questionID == questionID)
                                  .questionOptions
                                  .length + 1;
    this.quizSection.find(x => x.sectionID == sectionID)
    .questions
    .find(y => y.questionID == questionID)
    .questionOptions
    .push(newQuestionOption)
    let sectionIndex = this.quizSection.findIndex(x => x.sectionID == sectionID)
    let questionIndex = this.quizSection[sectionIndex]
                                      .questions
                                      .findIndex(y => y.questionID == questionID)
    if(this.quizSection[sectionIndex].questions[questionIndex].questionOptionsValidationFlag == false){
      this.removeQuizSectionQuestionOptionCountValidation(sectionIndex, questionIndex);
    }
  }

  removeSectionQuestionOption(sectionID: number, questionID: number, optionID: number){
    let sectionIndex = this.quizSection.findIndex(x => x.sectionID == sectionID)
    let questionIndex = this.quizSection[sectionIndex]
                                      .questions
                                      .findIndex(y => y.questionID == questionID)
    let optionIndex = this.quizSection[sectionIndex]
                                      .questions[questionIndex]
                                      .questionOptions
                                      .findIndex(z => z.optionID == optionID);
    this.quizSection[sectionIndex] 
    .questions[questionIndex]
    .questionOptions
    .splice(optionIndex,1);
    this.setSectionQuestionOptionIDs(sectionIndex, questionIndex);        

  }

  setSectionQuestionOptionIDs(sectionIndex: number, questionIndex: number){
    
    for(var i = 0; i< this.quizSection[sectionIndex].questions[questionIndex].questionOptions.length; i++){
      this.quizSection[sectionIndex].questions[questionIndex].questionOptions[i].optionID = i + 1;
    }
  }

  removeSectionQuestion(sectionID: number, questionID: number){
    let sectionIndex = this.quizSection.findIndex(x => x.sectionID == sectionID)

    let questionIndex = this.quizSection[sectionIndex]
                                      .questions
                                      .findIndex(y => y.questionID == questionID);
    this.quizSection[sectionIndex] 
    .questions
    .splice(questionIndex,1);
    this.setSectionQuestionIDs(sectionIndex);           

  }

  setSectionQuestionIDs(sectionIndex: number){
    for(var i = 0; i< this.quizSection[sectionIndex].questions.length; i++){
      this.quizSection[sectionIndex].questions[i].questionID = i + 1;
    }
  }

  removeSection(sectionID: number){
    let sectionIndex = this.quizSection.findIndex(x => x.sectionID == sectionID)
    this.quizSection
    .splice(sectionIndex,1);
    this.setSectionIDs();           

  }

  setSectionIDs(){
    for(var i = 0; i< this.quizSection.length; i++){
      this.quizSection[i].sectionID = i + 1;
    }
  }

  createQuiz(){
    this.checkAllValidations();
    if(!this.validationFlag){
      return;
    }
    this.quiz.sections = this.quizSection;
    let quizID = this.storageService.addQuiz(this.quiz)
    this.router.navigate(['College', 'Quiz', quizID])

  }
  checkAllValidations(){
    this.validationFlag = true;

    this.checkQuizDetailValidations();
    this.checkSectionValidations();
    this.checkQuestionValidations();
    this.checkQuestionOptionValidations();
  }

  checkQuizDetailValidations(){
    this.quiz.quizLabelValidationFlag = true;
    this.quiz.quizLabelValidationMessage = '';
    this.quiz.sectionsValidationFlag = true;
    this.quiz.sectionsValidationMessage = '';
    this.quiz.totalMarksValidationFlag = true;
    this.quiz.totalMarksValidationMessage = '';
    this.quiz.subjectIDValidationFlag = true;
    this.quiz.subjectIDValidationMessage = '';

    if(this.quiz.quizLabel == undefined || this.quiz.quizLabel == null){
      this.quiz.quizLabelValidationFlag = false;
      this.quiz.quizLabelValidationMessage = 'Provide Quiz Name';
      this.validationFlag = false;
    }
    else if(this.quiz.quizLabel.trim() == ''){
      this.quiz.quizLabelValidationFlag = false;
      this.quiz.quizLabelValidationMessage = 'Provide quiz name';
      this.validationFlag = false;
      this.quiz.quizLabel = '';
    }

    if(this.quiz.subjectID == undefined || this.quiz.subjectID == null){
      this.quiz.subjectIDValidationFlag = false;
      this.quiz.subjectIDValidationMessage = 'Select quiz subject';
      this.validationFlag = false;
    }
    
    if(this.quiz.totalMarks == undefined || this.quiz.totalMarks == null){
      this.quiz.totalMarksValidationFlag = false;
      this.quiz.totalMarksValidationMessage = 'Set quiz marks';
      this.validationFlag = false;
    }
    else if(this.quiz.totalMarks <= 0){
      this.quiz.totalMarksValidationFlag = false;
      this.quiz.totalMarksValidationMessage = 'Should not be negative';
      this.validationFlag = false;
    }
    if(this.quizSection == undefined || this.quizSection == null){
      this.quiz.sectionsValidationFlag = false;
      this.quiz.sectionsValidationMessage = 'Add Section(s)';
      this.validationFlag = false;
    }
    else if(this.quizSection.length <= 0){
      this.quiz.sectionsValidationFlag = false;
      this.quiz.sectionsValidationMessage = 'Add Section(s)';
      this.validationFlag = false;
    }

  }

  checkSectionValidations(){
    for(var i = 0; i < this.quizSection.length; i++){

        this.quizSection[i].sectionLabelValidationFlag = true;
        this.quizSection[i].sectionLabelValidationMessage = '';
        this.quizSection[i].weightageValidationFlag = true;
        this.quizSection[i].weightageValidationMessage = '';
        this.quizSection[i].questionsValidationFlag = true;
        this.quizSection[i].questionsValidationMessage = '';

      if(this.quizSection[i].sectionLabel == undefined || this.quizSection[i].sectionLabel == undefined){
        this.quizSection[i].sectionLabelValidationFlag = false;
        this.quizSection[i].sectionLabelValidationMessage = 'Section name is required';
        this.validationFlag = false;
      }
      else if(this.quizSection[i].sectionLabel.trim() == ''){
        this.quizSection[i].sectionLabelValidationFlag = false;
        this.quizSection[i].sectionLabelValidationMessage = 'Section name is required';
        this.validationFlag = false;
        this.quizSection[i].sectionLabel = '';
      }
      if(this.quizSection[i].weightage == undefined || this.quizSection[i].weightage == null){
        this.quizSection[i].weightageValidationFlag = false;
        this.quizSection[i].weightageValidationMessage = 'Weightage is required';  
        this.validationFlag = false;    
      }
      else if(this.quizSection[i].weightage < 0){
        this.quizSection[i].weightageValidationFlag = false;
        this.quizSection[i].weightageValidationMessage = 'Should not be negative';
        this.validationFlag = false;
            }
      if(this.quizSection[i].questions.length == 0){
        this.quizSection[i].questionsValidationFlag = false;
        this.quizSection[i].questionsValidationMessage = 'Atleast one question should be there in a section';
        this.validationFlag = false;
      }    
    }

  }

  checkQuestionValidations(){
    for(var i = 0; i < this.quizSection.length; i++){
      for(var j = 0; j < this.quizSection[i].questions.length; j++){

          this.quizSection[i].questions[j].questionDescriptionValidationFlag = true;
          this.quizSection[i].questions[j].questionDescriptionValidationMessage = '';
          this.quizSection[i].questions[j].questionTypeValidationFlag = true;
          this.quizSection[i].questions[j].questionTypeValidationMessage = '';
          this.quizSection[i].questions[j].questionOptionsValidationFlag = true;
          this.quizSection[i].questions[j].questionOptionsValidationMessage = '';
        
        if(this.quizSection[i].questions[j].questionDescription == undefined || this.quizSection[i].questions[j].questionDescription == undefined){
          this.quizSection[i].questions[j].questionDescriptionValidationFlag = false;
          this.quizSection[i].questions[j].questionDescriptionValidationMessage = 'Question is required';
          this.validationFlag = false;
        }
        else if(this.quizSection[i].questions[j].questionDescription.trim() == ''){
          this.quizSection[i].questions[j].questionDescriptionValidationFlag = false;
          this.quizSection[i].questions[j].questionDescriptionValidationMessage = 'Question is required';
          this.validationFlag = false;
          this.quizSection[i].questions[j].questionDescription = ''
        }
        if(this.quizSection[i].questions[j].questionType == undefined || this.quizSection[i].questions[j].questionType == null){
          this.quizSection[i].questions[j].questionTypeValidationFlag = false;
          this.quizSection[i].questions[j].questionTypeValidationMessage = 'Select question type';
          this.validationFlag = false;
        }
        else if(this.quizSection[i].questions[j].questionType < 1 || this.quizSection[i].questions[j].questionType > 2){
          this.quizSection[i].questions[j].questionTypeValidationFlag = false;
          this.quizSection[i].questions[j].questionTypeValidationMessage = 'Invalid question type';
          this.validationFlag = false;
        }  
        if(this.quizSection[i].questions[j].questionOptions.length == 0){
          this.quizSection[i].questions[j].questionOptionsValidationFlag = false;
          this.quizSection[i].questions[j].questionOptionsValidationMessage = 'Provide options for question'
          this.validationFlag = false;
        }  
      }
    }
  }

  checkQuestionOptionValidations(){
    for(var i = 0; i < this.quizSection.length; i++){
      for(var j = 0; j < this.quizSection[i].questions.length; j++){
        for(var k = 0; k < this.quizSection[i].questions[j].questionOptions.length; k++){
            
          this.quizSection[i].questions[j].questionOptions[k].optionLabelValidationFlag = true;
          this.quizSection[i].questions[j].questionOptions[k].optionLabelValidationMessage = '';
          
          if(this.quizSection[i].questions[j].questionOptions[k].optionLabel == undefined || this.quizSection[i].questions[j].questionOptions[k].optionLabel == undefined){
            this.quizSection[i].questions[j].questionOptions[k].optionLabelValidationFlag = false;
            this.quizSection[i].questions[j].questionOptions[k].optionLabelValidationMessage = 'Provide option value';
            this.validationFlag = false;
          }
          else if(this.quizSection[i].questions[j].questionOptions[k].optionLabel.trim() == ''){
            this.quizSection[i].questions[j].questionOptions[k].optionLabelValidationFlag = false;
            this.quizSection[i].questions[j].questionOptions[k].optionLabelValidationMessage = 'Provide option value';
            this.validationFlag = false;
            this.quizSection[i].questions[j].questionOptions[k].optionLabel = '';
          }
        }
      }
    }
  }

  removeQuizSectionCountValidation(){
    this.quiz.sectionsValidationFlag = true;
    this.quiz.sectionsValidationMessage = '';
  }

  removeQuizSectionQuestionCountValidation(sectionIndex: number){
    this.quizSection[sectionIndex].questionsValidationFlag = true;
    this.quizSection[sectionIndex].questionsValidationMessage = '';
  }
  removeQuizSectionQuestionOptionCountValidation(sectionIndex: number, questionIndex: number){
    this.quizSection[sectionIndex].questions[questionIndex].questionOptionsValidationFlag = true;
    this.quizSection[sectionIndex].questions[questionIndex].questionOptionsValidationMessage = '';
  }

}


