import { Component, OnInit, ViewChild } from '@angular/core';
import { LectureNotes } from 'src/app/Models/lecture-notes';
import {MatTableDataSource} from '@angular/material/table';
import { StorageService } from 'src/app/storage.service';
import { MatSort } from '@angular/material';

@Component({
  selector: 'app-lecture-notes',
  templateUrl: './lecture-notes.component.html',
  styleUrls: ['./lecture-notes.component.css']
})
export class LectureNotesComponent implements OnInit {
  selectedDate: Date;

  userRole: number;
  lectureNotes: Array<LectureNotes>;

  displayedColumnsForTeacher: string[] = ['subjectName', 'departmentName', 'notes'];
  displayedColumnsForStudent: string[] = ['subjectName', 'teacherName', 'notes'];
  dataSource :MatTableDataSource<LectureNotes>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  addNotes: Array<boolean>;
  editFlag: Array<boolean>;
  
  editNotes: Array<string>;
  note: string;
  message: Array<string>;

  constructor(private storageService: StorageService) { 
    this.selectedDate = new Date();
    this.lectureNotes = new Array<LectureNotes>();
    
  }

  ngOnInit() {
    this.userRole = this.storageService.getUserRole();
    if(this.userRole == 1){
      this.getLectureNotesForStudent()
    }
    else{
      this.getLectureNotes();
    }
  }

  createEditNotes(){
    this.addNotes = new Array<boolean>();  
    this.editFlag = new Array<boolean>();  
    this.editNotes = new Array<string>();
    for(var i = 0; i< this.lectureNotes.length; i++){
      this.editNotes.push('');
      this.editFlag.push(false);
      if(this.lectureNotes[i].notes == undefined){
        this.addNotes.push(true)
      }
      else{
        this.addNotes.push(false)
      }
    }
  }

  getSelectedDateNotes(){
    setTimeout(this.getLectureNotes, 0);
  }
  
  getLectureNotes(){
    this.lectureNotes = this.storageService.getLectureNotesOfTeacher(this.selectedDate)
    this.dataSource = new MatTableDataSource<LectureNotes>(this.lectureNotes);
    this.dataSource.sort = this.sort;
    if(this.lectureNotes !=null){
      this.createEditNotes();
    }
    
  }


  editLectureNotes(index: number){
    this.editFlag[index] = true;
    if(this.lectureNotes[index].notes != undefined){
      this.editNotes[index] = this.lectureNotes[index].notes;
    }
  }
  saveNotes(index: number){
    //this.note = 
    this.message = new Array<string>();
    if(this.editNotes[index].trim() == ''){
      this.message.push('Provide some notes.')
      return
    }
    this.lectureNotes[index].notes = this.editNotes[index].trim()
    this.storageService.saveNotesOfTeacher(this.lectureNotes[index]);
    this.editFlag[index] = false;
    this.addNotes[index] = false;

  }
  revertNotes(index: number){
    this.message = [];
    this.editNotes[index] = '';
    this.editFlag[index] = false;
    console.log(this.lectureNotes[index].notes)
    if(this.lectureNotes[index].notes == undefined){
      this.addNotes[index] = true;
    }
    else{
      this.addNotes[index] = false;
    }
  }

  getLectureNotesForStudent(){
    this.lectureNotes = this.storageService.getLectureNotesForStudent(this.selectedDate);
    this.dataSource = new MatTableDataSource<LectureNotes>(this.lectureNotes);
    this.dataSource.sort = this.sort;
  }
}
