import { Component, OnInit } from '@angular/core';
import { StorageService } from './storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'ggk-college';
  constructor(private storageService: StorageService){
  }
  ngOnInit(){
    this.setDataToLocalStorage();

  }
  private setDataToLocalStorage(){
    this.storageService.setRoles();
    this.storageService.setMenuItems();
    this.storageService.setDepartments();
    this.storageService.setDepartmentSubjects();
    this.storageService.setCollegeTimings();
    this.storageService.setStudents();
    this.storageService.setTeachers();
    this.storageService.setStudentDepartmentsTimeTable();

  }
}
