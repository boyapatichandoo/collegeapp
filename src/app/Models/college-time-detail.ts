import { Time } from '@angular/common';

export class CollegeTimeDetail {
    timeID: number;
    startTime: Time;
    endTime: Time;
    constructor(timeID: number,startTime: Time,endTime: Time){
        this.timeID = timeID;
        this.startTime = startTime;
        this.endTime = endTime;
    }
}
