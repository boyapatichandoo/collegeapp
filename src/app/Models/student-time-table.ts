import { DepartmentTimeDetails } from './department-time-details';
import { WeekDay } from './week-day.enum';

export class StudentTimeTable {
    studentTimeTableID: number;
    departmentID: number;
    weekDay: WeekDay;
    timeDetails: Array<DepartmentTimeDetails>;
    constructor(timeTableID?: number, departmentID?: number, weekDay?: WeekDay, timeDetails?: Array<DepartmentTimeDetails>){
        this.studentTimeTableID = timeTableID;
        this.departmentID = departmentID;
        this.timeDetails = timeDetails;
        this.weekDay = weekDay;
    }

}
