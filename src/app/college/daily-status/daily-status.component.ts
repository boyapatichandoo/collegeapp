import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { StorageService } from 'src/app/storage.service';
import { TeacherStatus } from 'src/app/Models/teacher-status';

@Component({
  selector: 'app-daily-status',
  templateUrl: './daily-status.component.html',
  styleUrls: ['./daily-status.component.css']
})
export class DailyStatusComponent implements OnInit {
  selectedDate: Date;

  dailyStatusNotes: Array<TeacherStatus>;

  displayedColumns: string[] = ['statusNotes'];
  dataSource :MatTableDataSource<TeacherStatus>;

  addNotes: Array<boolean>;
  editFlag: Array<boolean>;
  
  editNotes: Array<string>;
  note: string;
  message: Array<string>;

  constructor(private storageService: StorageService) { 
    this.selectedDate = new Date();
    this.dailyStatusNotes = new Array<TeacherStatus>();
    
  }

  ngOnInit() {
    this.getDailyStatus();
  }

  createEditNotes(){
    this.addNotes = new Array<boolean>();  
    this.editFlag = new Array<boolean>();  
    this.editNotes = new Array<string>();
    for(var i = 0; i< this.dailyStatusNotes.length; i++){
      this.editNotes.push('');
      this.editFlag.push(false);
      if(this.dailyStatusNotes[i].statusNotes == undefined){
        this.addNotes.push(true)
      }
      else{
        this.addNotes.push(false)
      }
    }
  }

  
  getDailyStatus(){
    this.dailyStatusNotes = this.storageService.getTechearDailyStatus(this.selectedDate)
    console.log(this.dailyStatusNotes)
    this.dataSource = new MatTableDataSource<TeacherStatus>(this.dailyStatusNotes);
    if(this.dailyStatusNotes !=null){
      this.createEditNotes();
    }
    
  }


  editLectureNotes(index: number){
    this.editFlag[index] = true;
    if(this.dailyStatusNotes[index].statusNotes != undefined){
      this.editNotes[index] = this.dailyStatusNotes[index].statusNotes;
    }
  }
  saveNotes(index: number){
    //this.note = 
    this.message = new Array<string>();
    if(this.editNotes[index].trim() == ''){
      this.message.push('Provide some notes.')
      return
    }
    this.dailyStatusNotes[index].statusNotes = this.editNotes[index].trim()
    this.storageService.saveDailyStatusOfTeacher(this.dailyStatusNotes[index]);
    this.editFlag[index] = false;
    this.addNotes[index] = false;

  }
  revertNotes(index: number){
    this.message = [];
    this.editNotes[index] = '';
    this.editFlag[index] = false;
    console.log(this.dailyStatusNotes[index].statusNotes)
    if(this.dailyStatusNotes[index].statusNotes == undefined){
      this.addNotes[index] = true;
    }
    else{
      this.addNotes[index] = false;
    }
  }
}

