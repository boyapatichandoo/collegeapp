import { QuestionSection } from './question-section';

export class Quiz {
    quizID: number;
    quizLabel: string;
    sections: Array<QuestionSection>;
    totalMarks: number;
    subjectID: number;

    answersGiven: boolean = false;

    quizLabelValidationFlag:boolean = true;
    quizLabelValidationMessage: string;

    sectionsValidationFlag:boolean = true;
    sectionsValidationMessage: string;

    totalMarksValidationFlag:boolean = true;
    totalMarksValidationMessage: string;

    subjectIDValidationFlag:boolean = true;
    subjectIDValidationMessage: string;
    constructor(){
        this.sections = new Array<QuestionSection>();
    }
}
