import { Component, OnInit } from '@angular/core';
import { StorageService } from '../storage.service';
import { User } from '../Models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  ngOnInit() {
  }
  loginView: boolean = true;

  showButton(){
    this.loginView = !this.loginView;
  }
}
