import { OptionAnswer } from './option-answer';

export class QuestionAnswer {
    questionID: number;
    radioTypeanswers: string;
    checkBoxTypeAnswers: Array<OptionAnswer>;
}
