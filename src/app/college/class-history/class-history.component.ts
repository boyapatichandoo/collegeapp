import { Component, OnInit, ViewChild } from '@angular/core';
import { StorageService } from 'src/app/storage.service';
import { StudentClassHistory } from 'src/app/Models/student-class-history';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { MatSort } from '@angular/material';

@Component({
  selector: 'app-class-history',
  templateUrl: './class-history.component.html',
  styleUrls: ['./class-history.component.css']
})
export class ClassHistoryComponent implements OnInit {

  classHistoryList = new Array<StudentClassHistory>();
  displayedColumns: string[] = ['classDate', 'subjectName','teacherName', 'status'];
  dataSource :MatTableDataSource<StudentClassHistory>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private storageService: StorageService) { }

  ngOnInit() {
    this.getClassHistory();
  }

  getClassHistory(){
    this.classHistoryList = this.storageService.getStudentClassHistory()
    this.dataSource = new MatTableDataSource<StudentClassHistory>(this.classHistoryList);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
