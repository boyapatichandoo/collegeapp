import { QuestionTypeEnum } from './question-type-enum.enum';
import { QuestionOption } from './question-option';

export class Question {
    questionID: number;
    questionType: QuestionTypeEnum;
    questionDescription: string;
    questionOptions: Array<QuestionOption>;

    questionDescriptionValidationMessage: string;
    questionDescriptionValidationFlag: boolean = true;

    questionTypeValidationMessage: string;
    questionTypeValidationFlag: boolean = true;

    questionOptionsValidationMessage: string;
    questionOptionsValidationFlag: boolean = true;
    constructor(){
        this.questionOptions = new Array<QuestionOption>();
    }
}
