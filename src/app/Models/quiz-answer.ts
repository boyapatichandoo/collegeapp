import { SectionAnswer } from './section-answer';

export class QuizAnswer {
    quizID: number;
    sections: Array<SectionAnswer>;
}
