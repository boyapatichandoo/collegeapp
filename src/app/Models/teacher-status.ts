export class TeacherStatus {
    teacherStatusID: number;
    teacherID: number;
    statusNotes: string;
    statusDate: string;
}
