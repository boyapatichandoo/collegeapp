import { EducationDetail } from './education-detail';
import { BasicDetail } from './basic-detail';

export class Student {
    studentID: number;
    basicDetails: BasicDetail;
    educationDetials: EducationDetail;
    IsActive: boolean;
}
