import { Injectable } from '@angular/core';
import { Router, CanActivate} from '@angular/router';
import { StorageService } from './storage.service';
@Injectable({
  providedIn: 'root'
})
export class AuthorizationService implements CanActivate {
  constructor(private storageService: StorageService, private router: Router) {}
  canActivate(): boolean {
    if (!this.storageService.isLoggedIn()) {
      this.router.navigate(['SignIn']);
      return false;
    }
    return true;
  }
}
