export class DepartmentSubject {
    subjectID: number;
    subjectName: string;
    departmentID: number;
    constructor(subjectID: number, subjectName: string, departmentID: number){
        this.subjectID = subjectID;
        this.subjectName = subjectName;
        this.departmentID = departmentID;
    }
}
