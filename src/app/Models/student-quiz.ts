import { QuizAnswer } from './quiz-answer';

export class StudentQuiz {
    studentID: number;
    quizID: number;
    answers: QuizAnswer;
}
