export class Department {
    departmentID: number;
    departmentName: string;

    constructor(departmentID:number, departmentName: string){
        this.departmentID =  departmentID;
        this.departmentName =  departmentName;
    }
}
