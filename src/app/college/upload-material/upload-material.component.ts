import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeHtml, SafeStyle, SafeScript, SafeUrl, SafeResourceUrl } from '@angular/platform-browser';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-upload-material',
  templateUrl: './upload-material.component.html',
  styleUrls: ['./upload-material.component.css']
})
export class UploadMaterialComponent implements OnInit {


  imageToUpload: File = null;
  selectedPdf: string;

  buttonText = 'Choose a PDF';
  selecetedFile: File;
  constructor(public sanitizer: DomSanitizer, private http: HttpClient) { }

  ngOnInit() {
  }

  onSelectPdfFile(event) {
    if (event.target.files && event.target.files[0]) {
      this.imageToUpload = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(this.imageToUpload);
      reader.onload = e => this.selectedPdf = reader.result.toString();
      console.log(this.selectedPdf)
      this.buttonText = event.target.files[0].name;
    }
  }
  onFileUpload(event){
    this.selecetedFile = event.target.files[0];
    }
    OnUploadFile() {
    //Upload file here send a binary data
    this.http.post('http://localhost:4200/src/assets', this.selecetedFile)
    .subscribe(res => console.log(res));
    }
}
