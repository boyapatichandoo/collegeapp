
export class User {
    userID: number;
    email:string;
    password: string;
    belongsToID: number;
    isTeacher: boolean;
    role: number;
}
