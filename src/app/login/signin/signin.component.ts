import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Models/user';
import { StorageService } from 'src/app/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  user: User;
  errorMessage: string;
  constructor(private storageSevice: StorageService, private router: Router) { 
    this.user = new User();
    this.errorMessage = null;
  }
  ngOnInit() {
  }
  
  verfiyLogin(){
    if(this.user != null && this.user != undefined){
      let status = this.storageSevice.checkUserCred(this.user)
      if(status.isSuccess){
        this.errorMessage = null;
        this.router.navigate(['College'])
      }
      else{
        this.errorMessage = status.message;
      }
    }
  }

}
