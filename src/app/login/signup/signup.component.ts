import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, RequiredValidator} from '@angular/forms';
import { Teacher } from '../../Models/teacher';
import { BasicDetail } from '../../Models/basic-detail';
import { ExperienceDetail } from '../../Models/experience-detail';
import { EducationDetail } from '../../Models/education-detail';
import { Student } from '../../Models/student';
import { StorageService } from '../../storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  signUpForm = new FormGroup({
  firstName: new FormControl('', [Validators.required]),
  lastName: new FormControl('', [Validators.required]),
  emailID: new FormControl('', [Validators.required, Validators.email]),
  password: new FormControl('', [Validators.required]),
  addressDetails: new FormControl('', [Validators.required]),
  addressArea: new FormControl('', [Validators.required]),
  city: new FormControl('', [Validators.required]),
  state: new FormControl('', [Validators.required]),
  pincode: new FormControl('', [Validators.required]),
  schoolPercentage: new FormControl('', [Validators.required]),
  interPercentage: new FormControl(''),
  ugPercentage: new FormControl(''),
  subject: new FormControl(''),
  teacherExperience: new FormControl('')
  })
  departmentID: number = 1;
  isTeacher: boolean = false;
  isLinear: boolean = true;
  constructor(private storageService: StorageService, private router: Router) { 

  }

  ngOnInit() {
  }
  createAccount(){
    console.log(this.validateFormData())
    if(!this.validateFormData()){
      return false;
    }
    let basicDetails: BasicDetail = new BasicDetail();
    basicDetails.firstName = this.signUpForm.value.firstName;
    basicDetails.lastName = this.signUpForm.value.lastName;
    basicDetails.profileName = this.signUpForm.value.firstName + ' ' + this.signUpForm.value.lastName;
    basicDetails.emailID = this.signUpForm.value.emailID;
    basicDetails.addressDetails = this.signUpForm.value.addressDetails;
    basicDetails.addressArea= this.signUpForm.value.addressArea;
    basicDetails.city = this.signUpForm.value.city;
    basicDetails.state = this.signUpForm.value.state;
    basicDetails.pincode = this.signUpForm.value.pincode;
    let education: EducationDetail = new EducationDetail();
    education.schoolPercentage = this.signUpForm.value.schoolPercentage;
    education.intermediatePercentage = this.signUpForm.value.intermediatePercentage;
    education.ugPercentage = this.signUpForm.value.ugPercentage;
    if(this.isTeacher){
      let experience: ExperienceDetail = new ExperienceDetail();
      experience.subject = this.signUpForm.value.subject;
      experience.experience = this.signUpForm.value.experience;
      let teacher: Teacher = new Teacher();
      teacher.basicDetails = basicDetails ;
      teacher.educationDetials = education ;
      teacher.experienceDetails = experience;
      teacher.IsActive = true;
      this.storageService.addTeacher(teacher,this.signUpForm.value.password);
    }
    else if(!this.isTeacher){
      let student: Student = new Student();
      student.basicDetails = basicDetails ;
      student.educationDetials = education ;
      student.IsActive = true;
      this.storageService.addStudent(student, this.signUpForm.value.password, this.departmentID)
    }
    console.log(this.signUpForm)
    this.signUpForm.reset();
    this.router.navigate(['/SignIn'])

  }

  validateFormData(){
    // console.log(this.signUpForm.valid)
    if(this.isTeacher){
      this.signUpForm.controls['interPercentage'].setValidators([Validators.required])
      this.signUpForm.controls['interPercentage'].updateValueAndValidity()
      this.signUpForm.controls['ugPercentage'].setValidators([Validators.required])
      this.signUpForm.controls['ugPercentage'].updateValueAndValidity();
    }
    else{
      this.signUpForm.controls['interPercentage'].setValidators(null)
      this.signUpForm.controls['interPercentage'].updateValueAndValidity()
      this.signUpForm.controls['ugPercentage'].setValidators(null);
      this.signUpForm.controls['ugPercentage'].updateValueAndValidity();
    }
    this.validateAllFormFields(this.signUpForm)

    return this.signUpForm.valid

  }
  validateAllFormFields(formGroup: FormGroup) {         
  Object.keys(formGroup.controls).forEach(field => {  
    const control = formGroup.get(field);             
    if (control instanceof FormControl) {             
      control.markAsTouched({ onlySelf: true });
      control.setValue(control.value.trim());
    } 
  });
}
}
