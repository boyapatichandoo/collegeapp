import { Time } from '@angular/common';

export class StudentTimeTableResponse {
    subjectID: number;
    subjectName: string;
    teacherID: number;
    teacherName: string;
    From: Time;
    To: Time;
}
