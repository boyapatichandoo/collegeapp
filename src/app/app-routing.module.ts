import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './login/signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './college/dashboard/dashboard.component';
import { SigninComponent } from './login/signin/signin.component';
import { QuestionComponent } from './college/quiz/question/question.component';
import { TimeTableComponent } from './college/time-table/time-table.component';
import { CreateQuestionComponent } from './college/quiz/create-question/create-question.component';
import { QuizComponent } from './college/quiz/quiz/quiz.component';
import { UploadMaterialComponent } from './college/upload-material/upload-material.component';
import { ClassHistoryComponent } from './college/class-history/class-history.component';
import { LectureNotesComponent } from './college/lecture-notes/lecture-notes.component';
import { DailyStatusComponent } from './college/daily-status/daily-status.component';
import { AuthorizationService } from './authorization.service';
import { RouteGuardService } from './route-guard.service';
import { UnaccessibleComponent } from './unaccessible/unaccessible.component';


const routes: Routes = [
  // { path: 'quiz/:id', component: QuestionComponent },
  {path:'', component:LoginComponent,
  children: [
    {
        path: 'SignIn',    
        component: SigninComponent
    },
    {
      path: 'SignUp',    
      component: SignupComponent
  },
    {
      path: '',        
      redirectTo: 'SignIn', pathMatch: 'full',
  },]},
  {path:'College', component:DashboardComponent,
  children: [
    {
        path: 'TimeTable',    
        component: TimeTableComponent,
        canActivate:[AuthorizationService, RouteGuardService]
    },
    {
      path: 'Quiz',    
      component: QuizComponent,
      canActivate:[AuthorizationService, RouteGuardService]
    },
    {
      path: 'Quiz/:id',    
      component: QuestionComponent,
      canActivate:[AuthorizationService, RouteGuardService]
    },
    {
      path: 'upload',    
      component: UploadMaterialComponent,
      canActivate:[AuthorizationService, RouteGuardService]
    },
    {
      path: 'history',    
      component: ClassHistoryComponent,
      canActivate:[AuthorizationService, RouteGuardService]
    },
    {
      path: 'lecturenotes',    
      component: LectureNotesComponent,
      canActivate:[AuthorizationService, RouteGuardService]
    },
    {
      path: 'dailystatus',    
      component: DailyStatusComponent,
      canActivate:[AuthorizationService, RouteGuardService]
    },
    {
      path: '',        
      redirectTo: 'TimeTable', pathMatch: 'full',
    },
    {
      path: 'CreateQuiz',        
     component:CreateQuestionComponent,
     canActivate:[AuthorizationService]
    },
    
]},
{
  path: 'no-access',        
 component:UnaccessibleComponent
    },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
