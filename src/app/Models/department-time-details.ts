import { CollegeTimeDetail } from './college-time-detail';

export class DepartmentTimeDetails {
    subjectID: number;
    teacherID: number;
    timeID: number;
    constructor(subjectID?: number,teacherID?: number,timeID?: number){
        this.subjectID = subjectID;
        this.teacherID = teacherID;
        this.timeID = timeID;
    }
}
