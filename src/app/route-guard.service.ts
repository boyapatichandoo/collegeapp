import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { Router, RouterStateSnapshot, CanActivate, ActivatedRouteSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardService implements CanActivate {
  constructor(private storageService: StorageService, private router: Router) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.storageService.isAccessibleRoute(state.url)) {
      this.router.navigate(['no-access']);
      return false;
    }
    return true;
  }
}
