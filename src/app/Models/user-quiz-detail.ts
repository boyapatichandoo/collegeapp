export class UserQuizDetail {
    quizID: number;
    quizLabel: string;
    answersProvided: boolean;
    quizTaken: boolean;
    marks: number;
    totalMarks: number;
}
