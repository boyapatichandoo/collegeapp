export class ClassAttendanceResponse {
    studentID: number;
    studentName: string;
    attended: boolean;
}
