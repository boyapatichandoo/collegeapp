import { Question } from './question';

export class QuestionSection {
    sectionID: number;
    sectionLabel: string;
    questions: Array<Question>;
    weightage: number;

    sectionLabelValidationMessage: string;
    sectionLabelValidationFlag: boolean = true;

    weightageValidationMessage: string;
    weightageValidationFlag: boolean = true;

    questionsValidationMessage: string;
    questionsValidationFlag: boolean = true;

    constructor(){
        this.questions = new Array<Question>();
    }
}
