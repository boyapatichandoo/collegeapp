import { UserQuizDetail } from './user-quiz-detail';

export class UserSubjectQuiz {
    subjectID: number;
    subjectName: string;
    quizDetails: Array<UserQuizDetail>
}
