import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnaccessibleComponent } from './unaccessible.component';

describe('UnaccessibleComponent', () => {
  let component: UnaccessibleComponent;
  let fixture: ComponentFixture<UnaccessibleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnaccessibleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnaccessibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
