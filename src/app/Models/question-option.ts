export class QuestionOption {
    optionID: number;
    optionLabel: string;
    optionLabelValidationMessage: string;
    optionLabelValidationFlag: boolean = true;
}
