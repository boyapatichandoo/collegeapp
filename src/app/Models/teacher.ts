import { EducationDetail } from './education-detail';
import { ExperienceDetail } from './experience-detail';
import { BasicDetail } from './basic-detail';

export class Teacher {
    teacherID: number;
    basicDetails: BasicDetail;
    educationDetials: EducationDetail;
    experienceDetails: ExperienceDetail;
    IsActive: boolean;
}
