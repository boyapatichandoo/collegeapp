export class ClassAttendance {
    classID: number;
    subjectID: number;
    studentID: number;
    attended: boolean;
    teacherID: number;
    classDate: string;
    constructor(){
    }
}
