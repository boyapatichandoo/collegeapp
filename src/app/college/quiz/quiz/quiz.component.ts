import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/storage.service';
import { UserSubjectQuiz } from 'src/app/Models/user-subject-quiz';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  quizDetails: Array<UserSubjectQuiz>;
  quizDetailsShow: Array<boolean>;
  userRole: number;
  constructor(private storageService: StorageService) { 
    this.quizDetails = new Array<UserSubjectQuiz>();
    this.quizDetailsShow = new Array<boolean>();
  }

  ngOnInit() {
    this.userRole = this.storageService.getUserRole();
    this.quizDetails = this.storageService.getUserSubjectQuizDetails();
    console.log(this.quizDetails);
  }
  setQuizDetailsShowList(){
    for(var i = 0; i< this.quizDetails.length; i++){
      this.quizDetailsShow.push(false);
    }
  }

  subjectQuizClick(index: number){
    this.quizDetailsShow[index] = !this.quizDetailsShow[index]
  }

}
