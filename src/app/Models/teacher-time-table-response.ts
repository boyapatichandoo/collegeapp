import { Time } from '@angular/common';

export class TeacherTimeTableResponse {
    subjectID: number;
    subjectName: string;
    departmentID: number;
    className: string;
    From: Time;
    To: Time;
    isAttendanceTaken: boolean = false;
}
